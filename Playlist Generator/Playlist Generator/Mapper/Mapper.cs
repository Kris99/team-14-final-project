﻿using AutoMapper;
using Playlist_Generator.Models;
using PlaylistGenerator.Data.Models;
using PlaylistGenerator.Data.Seeding.JsonModels;
using PlaylistGenerator.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Text;

namespace Playlist_Generator.Mapper
{
    public class Mapper : Profile
    {
        public Mapper()
        {
            CreateMap<JsonPlaylist, Playlist>();
            CreateMap<Playlist, JsonPlaylist>();

            CreateMap<IEnumerable<JsonPlaylist>, IEnumerable<Playlist>>();
            CreateMap<IEnumerable<Playlist>, IEnumerable<JsonPlaylist>>();

            CreateMap<JsonSong, Song>();
            CreateMap<Song, JsonSong>();

            CreateMap<UserDTO, User>();
            CreateMap<User, UserDTO>();

            CreateMap<RegisterViewModel, UserDTO>();
            CreateMap<LoginViewModel, UserDTO>();

            CreateMap<UserViewModel, User>().ReverseMap();
            CreateMap<ICollection<User>, ICollection<UserViewModel>>();

            CreateMap<UserViewModel, UserDTO>().ReverseMap();

            CreateMap<Song, SongDTO>().ReverseMap();
            CreateMap<PlaylistDTO, Playlist>().ReverseMap();

            CreateMap<PlaylistDTO, PlaylistViewModel>().ReverseMap();

            CreateMap<ArtistDTO, Artist>().ReverseMap();
            CreateMap<AlbumDTO, Album>().ReverseMap();
        }
    }
}
