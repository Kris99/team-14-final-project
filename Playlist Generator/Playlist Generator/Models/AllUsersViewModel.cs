﻿using System;
using System.Collections.Generic;

namespace Playlist_Generator.Models
{
    public class AllUsersViewModel
    {
        public ICollection<UserViewModel> Users { get; set; } = new List<UserViewModel>();
        public int Total
        {
            get
            {
                return this.Users.Count;
            }
        }

    }
}
