﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using PlaylistGenerator.Data.Models;

namespace Playlist_Generator.Models
{
    public class GeneratePlaylistViewModel
    {
        [DisplayName("Playlist title")]
        public string Title { get; set; }
        [DisplayName("First Genre")]
        public Genres FirstGenre { get; set; }
        [DisplayName("Percentage of first genre")]
        [Range(0, 100)]
        public long FirstGenrePercentage { get; set; }
        [DisplayName("Second Genre")]
        public Genres SecondGenre { get; set; }
        [Range(0, 100)]
        [DisplayName("Percentege of second genre")]
        public long SecondGenrePercentage { get; set; }

        public bool AllowDuplicateArtists { get; set; }
        public bool TopRatedTracks { get; set; }

        public BingDestinationsViewModel BingDestinationsViewModel { get; set; }
    }
}
