﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PlaylistGenerator.Services.DTOs;

namespace Playlist_Generator.Models
{
    public class PlaylistsViewModel
    {
        public IEnumerable<PlaylistDTO> Playlists { get; set; }

    }
}
