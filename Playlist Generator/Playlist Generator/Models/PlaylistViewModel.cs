﻿using PlaylistGenerator.Services.DTOs;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace Playlist_Generator.Models
{
    public class PlaylistViewModel
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string URL { get; set; }
        public string Genres { get; set; }
        [DisplayName("Time")]
        public int TotalPlayTime { get; set; }
        [DisplayName("Rank")]
        public double AvrRank { get; set; }
        
        public IEnumerable<SongDTO> Songs { get; set; }
    }
}
