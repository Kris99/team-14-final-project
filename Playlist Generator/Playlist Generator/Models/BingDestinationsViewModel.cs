﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Playlist_Generator.Models
{
    public class BingDestinationsViewModel
    {
        [Required(ErrorMessage = "Please choose a starting point")]
        public string FirstDestinationLat { get; set; }
        [Required(ErrorMessage = "Please choose a starting point")]
        public string FirstDestinationLon { get; set; }
        [Required(ErrorMessage = "Please choose an ending destination")]
        public string SecondDestinationLat { get; set; }
        [Required(ErrorMessage = "Please choose an ending destination")]
        public string SecondDestinationLon { get; set; } 
    }
}
