﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Playlist_Generator.Models;
using PlaylistGenerator.Services.Contracts;
using PlaylistGenerator.Services.DTOs;

namespace Playlist_Generator.Controllers
{
    public class LoginController : Controller
    {
        private readonly ILoginService service;
        private readonly IMapper mapper;

        public LoginController(ILoginService service, IMapper mapper)
        {
            this.service = service;
            this.mapper = mapper;
        }

        // GET Login/Index
        public IActionResult Index()
        {
            return View();
        }

        // POST Login/Index
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Index(LoginViewModel model)
        {
            if(ModelState.IsValid)
            {
                var userDTO = this.mapper.Map<UserDTO>(model);
                var result = await this.service.LoginUserAsync(userDTO);

                if(result == true)
                {
                    return RedirectToAction(nameof(Index), "Home");
                }

            }
                return View(model);
        }
    }
}
