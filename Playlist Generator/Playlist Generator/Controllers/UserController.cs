﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Playlist_Generator.Models;
using PlaylistGenerator.Services.Contracts;
using PlaylistGenerator.Services.DTOs;

namespace Playlist_Generator.Controllers
{
    public class UserController : Controller
    {
        private readonly IUserService service;
        private readonly IMapper mapper;

        public UserController(IUserService service, IMapper mapper)
        {
            this.service = service;
            this.mapper = mapper;
        }

        [Authorize(Roles = "Admin")]
        public IActionResult Index()
        {
            var model = new AllUsersViewModel();

            var users = this.service.GetAllUsers();

            foreach (var user in users)
            {
                var userModel = this.mapper.Map<UserViewModel>(user);

                model.Users.Add(userModel);
            }

            return View(model);
        }


        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Edit(Guid id)
        {
            var userDTO = await service.GetUserById(id);

            if(userDTO == null)
            {
                return NotFound();
            }

            var model = this.mapper.Map<UserViewModel>(userDTO);

            return View(model);
        }

        [HttpPost]
        [Authorize(Roles = "Admin")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(UserViewModel  model)
        {
            await service.UpdateUserAsync(model.Id, model.UserName, model.FirstName, model.LastName, model.Email);

            return RedirectToAction("Index");
        }
        
        
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }
        
            var userDTO = await this.service.GetUserById(id);
            if (userDTO == null)
            {
                return NotFound();
            }
        
            UserViewModel viewModel = this.mapper.Map<UserViewModel>(userDTO);
        
            return View(viewModel);
        }

        [Authorize(Roles = "Admin")]
        public IActionResult Create()
        {
            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(RegisterViewModel model)
        {

            if (ModelState.IsValid)
            {
                var userDTO = this.mapper.Map<UserDTO>(model);
                bool result = await this.service.CreateUserAsync(userDTO);

                if (result == true)
                    return RedirectToAction(nameof(Index));
            }

            return View(model);
        }


        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Delete(Guid id)
        {
            var userDTO = await service.GetUserById(id);

            if(userDTO == null)
            {
                return NotFound();
            }

            var model = this.mapper.Map<UserViewModel>(userDTO);

            return View(model);
        }

        [Authorize(Roles = "Admin")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(UserViewModel model)
        {
            var result = await this.service.DeleteUserAsync(model.Id);

            if (result == true)
            {
                return RedirectToAction(nameof(Index));
            }

            return NotFound();
        }


        [Authorize(Roles ="Admin")]
        public async Task<IActionResult> BanUser(Guid? id)
        {
            var result = await this.service.BanUserAsync(id);

            if(result == true)
            {
                return RedirectToAction(nameof(Index));
            }

            return NotFound();
        }

        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> UnbanUser(Guid? id)
        {
            var result = await this.service.UnbanUserAsync(id);

            if (result == true)
            {
                return RedirectToAction(nameof(Index));
            }

            return NotFound();
        }

    }
}
