﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using PlaylistGenerator.Services.Contracts;

namespace Playlist_Generator.Controllers
{
    public class LogoutController : Controller
    {
        private readonly ILoginService service;

        public LogoutController(ILoginService service)
        {
            this.service = service;
        }

        // GET logout/index
        public IActionResult Index()
        {
            this.service.LogoutUserAsync();

            return RedirectToAction("Index", "Home");
        }
    }
}
