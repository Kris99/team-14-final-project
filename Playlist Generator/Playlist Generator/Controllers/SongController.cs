﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PlaylistGenerator.Services.Contracts;

namespace Playlist_Generator.Controllers
{
    public class SongController : Controller
    {
        private readonly ISongService service;
        private readonly IMapper mapper;

        public SongController(ISongService service, IMapper mapper)
        {
            this.service = service;
            this.mapper = mapper;
        }

        [Authorize(Roles = "User, Admin")]
        public async Task<IActionResult> Details(Guid id)
        {
            var song = await this.service.GetSong(id);

            return View(song);
        }
    }
}
