﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Playlist_Generator.Models;
using PlaylistGenerator.Data;
using PlaylistGenerator.Data.Models;

namespace Playlist_Generator.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly PlaylistGeneratorContext context;

        public HomeController(ILogger<HomeController> logger, PlaylistGeneratorContext context)
        {
            _logger = logger;
            this.context = context;
        }

        public IActionResult Index()
        {
            //var startURL = "http://api.deezer.com/search/playlist?q=pop";
            //
            //var client = new HttpClient();
            //
            //var response = await client.GetAsync(startURL);
            //var playlistCollectionJson = await response.Content.ReadAsStringAsync();
            //var playlistCollection = JsonSerializer.Deserialize<PlaylistCollection>(playlistCollectionJson);
            //
            //foreach (var playlist in playlistCollection.Playlists)
            //{
            //    playlist.Id = Guid.Empty;
            //    this.context.Playlists.Add(playlist);
            //}
            //this.context.SaveChanges();
            //
            //var firstPlaylist = playlistCollection.Playlists.First();
            //var songsUrl = firstPlaylist.URL;
            //var songCollectionResponse = await client.GetAsync(songsUrl);
            //var songCollectionJson = await songCollectionResponse.Content.ReadAsStringAsync();
            //var songCOllection = JsonSerializer.Deserialize<TracklistCollection>(songCollectionJson);
            //
            //return new JsonResult(songCOllection);

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

    }
}
