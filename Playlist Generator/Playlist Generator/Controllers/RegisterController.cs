﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Playlist_Generator.Models;
using PlaylistGenerator.Services.Contracts;
using PlaylistGenerator.Services.DTOs;

namespace Playlist_Generator.Controllers
{
    public class RegisterController : Controller
    {
        private readonly IRegisterService service;
        private readonly IMapper mapper;

        public RegisterController(IRegisterService service, IMapper mapper)
        {
            this.service = service;
            this.mapper = mapper;
        }

        // GET: Register/Index
        public IActionResult Index()
        {
            return View();
        }

        // POST: Register/Index 
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Index(RegisterViewModel model)
        {

            if (ModelState.IsValid)
            {
                var userDTO = mapper.Map<UserDTO>(model);
                bool result = await service.RegisterUserAsync(userDTO);

                if (result == true)
                    return RedirectToAction(nameof(Index), "Home");
            }

            return View(model);
        }

    }
}
