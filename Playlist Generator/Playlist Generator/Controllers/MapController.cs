﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Playlist_Generator.Models;
using PlaylistGenerator.Data.Models;
using PlaylistGenerator.Services.Contracts;
using PlaylistGenerator.Services.DTOs;

namespace Playlist_Generator.Controllers
{
    public class MapController : Controller
    {
        private readonly UserManager<User> userManager;
        private readonly IPlaylistService playlistService;

        public MapController(UserManager<User> userManager, IPlaylistService playlistService)
        {
            this.userManager = userManager;
            this.playlistService = playlistService;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        [Authorize(Roles = "User, Admin")]
        public async Task<IActionResult> GeneratePlaylist()
        {
            await Task.Delay(0);

            return View();
        }

        [HttpPost]
        [Authorize(Roles = "User, Admin")]
        public async Task<IActionResult> GeneratePlaylist(GeneratePlaylistViewModel generatePlaylistViewModel)
        {
            if(!ModelState.IsValid)
            {
                return View();
            }

            bool sameArtist = generatePlaylistViewModel.AllowDuplicateArtists;
            bool topRanked = generatePlaylistViewModel.TopRatedTracks;
            string title = generatePlaylistViewModel.Title;
            var user = await this.userManager.GetUserAsync(User);

            Dictionary<Genres, double> genrePercetages = new Dictionary<Genres, double>();

            genrePercetages.Add(generatePlaylistViewModel.FirstGenre, generatePlaylistViewModel.FirstGenrePercentage);
            if(generatePlaylistViewModel.FirstGenre != generatePlaylistViewModel.SecondGenre)
                genrePercetages.Add(generatePlaylistViewModel.SecondGenre, generatePlaylistViewModel.SecondGenrePercentage);
            
            HttpClient client = new HttpClient();
            var url = $"https://dev.virtualearth.net/REST/v1/Routes/DistanceMatrix?origins={generatePlaylistViewModel.BingDestinationsViewModel.FirstDestinationLat},{generatePlaylistViewModel.BingDestinationsViewModel.FirstDestinationLon}&destinations={generatePlaylistViewModel.BingDestinationsViewModel.SecondDestinationLat},{generatePlaylistViewModel.BingDestinationsViewModel.SecondDestinationLon}&travelMode=driving&key=Ag1-oHlx6OC_A4B6GFHeLGTcOZ8hYGIr-ifRsFRjGkFgXE3nLPe_jU1dXYy_MDk7";
            var response = await client.GetStringAsync(url);
            var jsonObject = JsonConvert.DeserializeObject<JsonBingResponse>(response);
            var duration = jsonObject.ResourceSets[0].Resources[0].Results[0].TravelDuration;

            var playlistDTO = await this.playlistService.GeneratePlaylistAsync((int)duration, sameArtist, topRanked, genrePercetages, user, title);


            return RedirectToAction("DetailsAfterGeneration", "Playlist", playlistDTO);
        }
    }
}