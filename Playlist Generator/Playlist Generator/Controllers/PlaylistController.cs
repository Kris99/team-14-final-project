﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Playlist_Generator.Models;
using PlaylistGenerator.Services.Contracts;
using PlaylistGenerator.Services.DTOs;

namespace Playlist_Generator.Controllers
{
    public class PlaylistController : Controller
    {
        private readonly IPlaylistService playlistService;
        private readonly IMapper mapper;

        public PlaylistController(IPlaylistService playlistService, IMapper mapper)
        {
            this.playlistService = playlistService;
            this.mapper = mapper;
        }

        // GET: PlaylistController
        public ActionResult Index()
        {
            var playlists = this.playlistService.GetAllPlaylists();

            PlaylistsViewModel model = new PlaylistsViewModel()
            { Playlists = playlists };

            return View(model);
        }

        // GET: PlaylistController/Details/5
        [Authorize(Roles = "User, Admin")]
        public async Task<ActionResult> Details(Guid id)
        {
            var playlist = await this.playlistService.GetPlaylist(id);

            var model = this.mapper.Map<PlaylistViewModel>(playlist);

            return View(model);
        }

        [Authorize(Roles = "User, Admin")]
        public ActionResult DetailsAfterGeneration(PlaylistDTO playlistDTO)
        {
            var model = this.mapper.Map<PlaylistViewModel>(playlistDTO);

            return View(nameof(Details), model);
        }


        // GET: PlaylistController/Edit/5
        [Authorize(Roles = "Admin")]
        public async Task<ActionResult> Edit(Guid id)
        {
            var playlist = await this.playlistService.GetPlaylist(id);

            return View(playlist);
        }

        // POST: PlaylistController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public async Task<ActionResult> Edit(PlaylistDTO playlist)
        {
            await this.playlistService.EditPlaylist(playlist);

            return RedirectToAction(nameof(Index));
        }

        // GET: PlaylistController/Delete/5
        [Authorize(Roles = "Admin")]
        public async Task<ActionResult> Delete(Guid id)
        {
            var playlist = await this.playlistService.GetPlaylist(id);

            if(playlist == null)
            {
                return NotFound();
            }

            return View(playlist);
        }

        // POST: PlaylistController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public async Task<ActionResult> Delete(PlaylistDTO playlistDTO)
        {
            var result = await this.playlistService.DeletePlaylist(playlistDTO.Id);

            if(result)
            {
                return RedirectToAction(nameof(Index));
            }

            return NotFound();
        }

        [Authorize(Roles = "User, Admin")]
        public async Task<ActionResult> Songs(Guid id)
        {
            var playlist = await this.playlistService.GetPlaylist(id);

            var model = this.mapper.Map<PlaylistViewModel>(playlist);

            model.Songs = this.playlistService.GetPlaylistsSongs(id);

            if (model.Songs == null)
            {
                return NotFound();
            }

            return View(model);
        }

    }
}
