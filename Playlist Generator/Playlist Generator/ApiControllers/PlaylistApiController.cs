﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using PlaylistGenerator.Data.Models;
using PlaylistGenerator.Services.Contracts;

namespace Playlist_Generator.ApiControllers
{
    [Route("api/playlist")]
    [ApiController]
    public class PlaylistApiController : ControllerBase
    {
        private readonly IPlaylistApiService service;

        public PlaylistApiController(IPlaylistApiService service)
        {
            this.service = service;
        }

        [HttpGet("all")]
        public async Task<IActionResult> GetAllPlaylists()
        {
            var playlists = await this.service.GetAllPlaylistsAsync();
            var authentication = HttpContext.User.Identity.IsAuthenticated;

            if (authentication)
                return Ok(playlists);

            return NotFound("No User is logged in.");

        }

        [HttpGet("")]
        public async Task<IActionResult> GetPlaylistsByGenre([FromQuery] string genre)
        {
            var playlists = await this.service.GetPlaylistsByGenreAsync(genre);
            var authentication = HttpContext.User.Identity.IsAuthenticated;

            if (authentication)
                return Ok(playlists);

            return NotFound("No User is logged in.");

        }


        [HttpGet("{id}/songs")]
        public async Task<IActionResult> GetPlaylistSongs(long id)
        {
            var songs = await this.service.GetPlaylistSongsAsync(id);
            var authentication = HttpContext.User.Identity.IsAuthenticated;

            if(authentication)
                return Ok(songs);

            return NotFound("No User is logged in.");

        }

        [HttpPut("{id}/edit")]
        public async Task<IActionResult> EditUsersPlaylist([FromQuery] string newTitle, long id)
        {
            var authentication = HttpContext.User.Identity.IsAuthenticated;

            if(authentication)
            {
                var playlist = await this.service.EditPlaylistAsync(id, newTitle);

                if (playlist == null)
                    return NotFound(playlist);

                return Ok(playlist);
            }

            return NotFound("No User is logged in.");
        }


        [HttpDelete("{id}/delete")]
        public async Task<IActionResult> DeleteUsersPlaylist(long id)
        {
            var authentication = HttpContext.User.Identity.IsAuthenticated;

            if (authentication)
            {
                var playlist = await this.service.DeletePlaylistAsync(id);

                return Ok(playlist);
            }

            return NotFound("No User is logged in.");
        }
    }
}
