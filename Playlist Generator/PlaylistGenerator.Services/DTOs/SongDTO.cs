﻿using PlaylistGenerator.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace PlaylistGenerator.Services.DTOs
{
    public class SongDTO
    {
        public Guid Id { get; set; }

        public string Title { get; set; }

        public int Duration { get; set; }

        public long Rank { get; set; }

        public string Url { get; set; }

        public ArtistDTO Artist { get; set; }

        public AlbumDTO Album { get; set; }

        public Genres Genre { get; set; }
        
        public Guid PlaylistId { get; set; }
    }
}
