﻿using PlaylistGenerator.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace PlaylistGenerator.Services.DTOs
{
    public class GenresPercentages
    {
        public Genres? Genre { get; set; }
        public float? Percentage { get; set; }
    }
}
