﻿using System;

namespace PlaylistGenerator.Services.DTOs
{
    public class ArtistDTO
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string TracklistUrl { get; set; }

        public string PictureUrl { get; set; }
    }
}