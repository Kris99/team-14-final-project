﻿using System;

namespace PlaylistGenerator.Services.DTOs
{
    public class AlbumDTO
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string TracklistUrl { get; set; }

        public string CoverUrl { get; set; }
    }
}