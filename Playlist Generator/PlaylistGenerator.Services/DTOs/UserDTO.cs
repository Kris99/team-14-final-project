﻿using PlaylistGenerator.Data.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace PlaylistGenerator.Services.DTOs
{
    public class UserDTO
    {
        private User user;

        public UserDTO()
        {

        }
        public UserDTO(User user)
        {
            this.user = user;
            Id = user.Id;
            FirstName = user.FirstName;
            LastName = user.LastName;
            UserName = user.UserName;
            Email = user.Email;        
        }

        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
        public bool IsBanned { get; set; }
        public DateTime? BanExpirationDate { get; set; }
    }

}
