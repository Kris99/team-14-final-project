﻿using PlaylistGenerator.Data.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace PlaylistGenerator.Services.DTOs
{
    public class PlaylistDTO
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string URL { get; set; }
        public string Genres { get; set; }
        [DisplayName("Duration")]
        public int TotalPlayTime { get; set; }
        [DisplayName("Rank")]
        public double AvrRank { get; set; }
        public IEnumerable<SongDTO> Songs { get; set; }
    }
}
