﻿using PlaylistGenerator.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PlaylistGenerator.Services.Contracts
{
    public interface IRegisterService
    {
        Task<bool> RegisterUserAsync(UserDTO userDTO);
    }
}
