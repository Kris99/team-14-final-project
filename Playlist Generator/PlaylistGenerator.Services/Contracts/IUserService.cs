﻿using PlaylistGenerator.Data.Models;
using PlaylistGenerator.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PlaylistGenerator.Services.Contracts
{
    public interface IUserService
    {
        Task<UserDTO> GetUserByUserNameAsync(string userName);
        ICollection<User> GetAllUsers();
        Task<UserDTO> GetUserById(Guid? id);
        Task<bool> CreateUserAsync(UserDTO userDTO);
        Task<bool> DeleteUserAsync(Guid? id);
        Task UpdateUserAsync(Guid id, string UserName, string FirstName, string LastName, string Email);
        Task<bool> BanUserAsync(Guid? id);
        Task<bool> UnbanUserAsync(Guid? id);

    }
}
