﻿using PlaylistGenerator.Data.Models;
using PlaylistGenerator.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PlaylistGenerator.Services.Contracts
{
    public interface ISongService
    {
        ICollection<Song> GetAllSongs();
        Task<SongDTO> GetSong(Guid id);
    }
}
