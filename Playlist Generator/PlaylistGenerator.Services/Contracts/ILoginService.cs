﻿using PlaylistGenerator.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PlaylistGenerator.Services.Contracts
{
    public interface ILoginService
    {
        Task<bool> LoginUserAsync(UserDTO userDTO);
        void LogoutUserAsync();
    }
}
