﻿using PlaylistGenerator.Data.Models;
using PlaylistGenerator.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PlaylistGenerator.Services.Contracts
{
    public interface IPlaylistService
    {
        Task<PlaylistDTO> GeneratePlaylistAsync(int duration, bool sameArtist, bool topSongs,
                Dictionary<Genres, double> genrePercentages, User user, string title);

        IEnumerable<PlaylistDTO> GetAllPlaylists();
        Task<PlaylistDTO> GetPlaylist(Guid id);
        Task<PlaylistDTO> EditPlaylist(PlaylistDTO playlist);
        Task<bool> DeletePlaylist(Guid id);
        IEnumerable<SongDTO> GetPlaylistsSongs(Guid playlistId);

    }
}
