﻿using PlaylistGenerator.Data.Models;
using PlaylistGenerator.Data.Seeding.JsonModels;
using PlaylistGenerator.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PlaylistGenerator.Services.Contracts
{
    public interface IPlaylistApiService
    {
        Task<IEnumerable<Playlist>> GetAllPlaylistsAsync();
        Task<PlaylistCollection> GetPlaylistsByGenreAsync(string genre);
        Task<SongCollection> GetPlaylistSongsAsync(long id);
        Task<JsonPlaylist> EditPlaylistAsync(long playlistId, string newTitle);
        Task<JsonPlaylist> DeletePlaylistAsync(long playlistId);

    }
}
