﻿using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using PlaylistGenerator.Data;
using PlaylistGenerator.Data.Models;
using PlaylistGenerator.Services.Contracts;
using PlaylistGenerator.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PlaylistGenerator.Services.Services
{
    public class LoginService : ILoginService
    {
        private readonly SignInManager<User> signInManager;
        private readonly PlaylistGeneratorContext context;

        public LoginService(SignInManager<User> signInManager, PlaylistGeneratorContext context)
        {
            this.signInManager = signInManager;
            this.context = context;
        }

        public async Task<bool> LoginUserAsync(UserDTO userDTO)
        {
            var result = await signInManager.PasswordSignInAsync(userDTO.UserName, userDTO.Password,
                                                                        isPersistent: true, lockoutOnFailure: false);

            return result.Succeeded;
        }

        public async void LogoutUserAsync()
        {
            await signInManager.SignOutAsync();
        }


    }
}
