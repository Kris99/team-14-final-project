﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using PlaylistGenerator.Data;
using PlaylistGenerator.Data.Models;
using PlaylistGenerator.Services.Contracts;
using PlaylistGenerator.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PlaylistGenerator.Services.Services
{
    public class PlaylistService : IPlaylistService
    {
        private readonly PlaylistGeneratorContext context;
        private readonly ISongService songService;
        private readonly IMapper mapper;

        public PlaylistService(PlaylistGeneratorContext context, ISongService songService, IMapper mapper)
        {
            this.context = context;
            this.songService = songService;
            this.mapper = mapper;
        }


        public IEnumerable<PlaylistDTO> GetTop3Playlists()
        {
            var playlists = this.context.Playlists.Where(pl => pl.IsDeleted == false).OrderBy(pl => pl.AvrRank).Take(3);

            var dtos = this.mapper.ProjectTo<PlaylistDTO>(playlists);

            return dtos;
        }

        public IEnumerable<PlaylistDTO> GetAllPlaylists()
        {

            var playlistsDTOs = this.context.Playlists.Where(pl => pl.IsDeleted == false).ProjectTo<PlaylistDTO>(mapper.ConfigurationProvider);

            return playlistsDTOs;
        }

        public async Task<PlaylistDTO> GetPlaylist(Guid id)
        {
            var playlist = await this.context.Playlists.FirstOrDefaultAsync(pl => pl.Id == id && pl.IsDeleted == false);

            if (playlist == null)
            {
                return null;
            }

            if(this.context.Playlists.Count() == 2)
            {
                return new PlaylistDTO() { Id = playlist.Id, Title = playlist.Title };
            }

            var dto = this.mapper.Map<PlaylistDTO>(playlist);

            return dto;
        }

        public async Task<PlaylistDTO> EditPlaylist(PlaylistDTO playlistDTO)
        {
            var playlist = await this.context.Playlists.FirstOrDefaultAsync(pl => pl.Id == playlistDTO.Id && pl.IsDeleted == false);

            if(playlist == null)
            {
                return null;
            }
            playlist.Title = playlistDTO.Title;
            playlist.ModifiedOn = DateTime.UtcNow;

            this.context.Update(playlist);
            await this.context.SaveChangesAsync();

            var dto = this.mapper.Map<PlaylistDTO>(playlist);

            return dto;
        }

        public async Task<bool> DeletePlaylist(Guid id)
        {
            var playlist = await this.context.Playlists.FirstOrDefaultAsync(pl => pl.Id == id && pl.IsDeleted == false);

            if (playlist == null)
            {
                return false;
            }

            playlist.IsDeleted = true;
            playlist.DeletedOn = DateTime.UtcNow;
            this.context.Update(playlist);
            await this.context.SaveChangesAsync();

            return true;
        }

        public async Task<PlaylistDTO> GeneratePlaylistAsync(int duration, bool sameArtist, bool topSongs,
                Dictionary<Genres, double> genrePercentages, User user, string title)
        {
            var allSongs = this.songService.GetAllSongs();
            var filteredSongs = FilterSongs(allSongs, sameArtist, topSongs, genrePercentages, duration * 60);

            if(filteredSongs == null)
            {
                return null;
            }

            string genresInString = string.Join(',', genrePercentages.Select(g => g.Key));

            Playlist playlist = new Playlist()
            {
                Id = Guid.NewGuid(),
                AddedOn = DateTime.UtcNow,
                AvrRank = Math.Round(filteredSongs.Select(s => s.Rank).Average(), 2),
                TotalPlayTime = filteredSongs.Select(s => s.Duration).Sum(),
                Genres = genresInString,
                Title = title,
                UserId = user.Id
            };

            playlist.URL = $"https://localhost:5001/Playlist/Details/{playlist.Id}";

            await this.context.Playlists.AddAsync(playlist);
            await this.context.SaveChangesAsync();

            playlist.Items = CreatePlaylistItems(filteredSongs, playlist).ToList();

            PlaylistDTO dto = this.mapper.Map<PlaylistDTO>(playlist);

            return dto;
        }

        public IEnumerable<SongDTO> GetPlaylistsSongs(Guid playlistId)
        {
            IEnumerable<PlaylistItem> items = GetPlaylistItems(playlistId);

            foreach (var item in items)
            {
                var song = this.context.Songs.FirstOrDefault(s => s.Id == item.SongId);

                var artist = this.context.Artists.FirstOrDefault(a => a.Id == song.ArtistId);
                var album = this.context.Albums.FirstOrDefault(a => a.Id == song.AlbumId);

                if (song != null)
                {
                    var dto = this.mapper.Map<SongDTO>(song);
                    dto.Album = this.mapper.Map<AlbumDTO>(album);
                    dto.Artist = this.mapper.Map<ArtistDTO>(artist);
                    yield return dto;
                }
            }

        }

        private ICollection<Song> FilterSongs(ICollection<Song> allSongs, bool sameArtist, bool topRank,
                                                            Dictionary<Genres, double> genrePercentages, int duration)
        {
            var genres = GetGenres(genrePercentages);
            ICollection<Song> genreTracks = new List<Song>();

            if (genres.Count() == 2)
            {
                foreach (var song in allSongs)
                {
                    if (song.Genre == genres.FirstOrDefault() || song.Genre == genres.LastOrDefault())
                    {
                        genreTracks.Add(song);
                    }
                }
            }
            else
            {
                genreTracks = allSongs;
            }

            if (topRank)
            {
                genreTracks.OrderBy(s => s.Rank);
            }
            else
            {
                genreTracks = Shuffle(genreTracks.ToList());
            }

            if (sameArtist)
            {
                var artistNames = new HashSet<string>();

                foreach (var song in allSongs)
                {
                    bool result = artistNames.Add(song.Artist.Name);

                    if (!result)
                    {
                        genreTracks.Remove(song);
                    }
                }
            }

            ICollection<Song> filteredSongs = new List<Song>();

            if (genres.Count() == 2)
            {
                double firstGenreDuration = duration * (genrePercentages.Values.First() / 100);
                double secondGenreDuration = duration * (genrePercentages.Values.Last() / 100);
                double firstGenreCurrent = 0.0;
                double secondGenreCurrent = 0.0;

                foreach (var song in genreTracks)
                {
                    if(firstGenreDuration <= firstGenreCurrent && secondGenreDuration <= secondGenreCurrent)
                    {
                        break;
                    }
                    if(song.Genre == genres.First() && firstGenreDuration > firstGenreCurrent)
                    {
                        firstGenreCurrent += song.Duration;
                        filteredSongs.Add(song);
                    }
                    else if (song.Genre == genres.Last() && secondGenreDuration > secondGenreCurrent)
                    {
                        secondGenreCurrent += song.Duration;
                        filteredSongs.Add(song);
                    }
                }
            }
            else
            {
                int totalDuration = 0;

                foreach (var song in genreTracks)
                {
                    if (totalDuration >= duration)
                    {
                        break;
                    }

                    totalDuration += song.Duration;
                    filteredSongs.Add(song);
                }
            }

            filteredSongs = Shuffle(filteredSongs.ToList());
            return filteredSongs;

        }
        private IEnumerable<Genres> GetGenres(Dictionary<Genres, double> genrePercentages)
        {
            var genres = genrePercentages.Select(g => g.Key);
            var allGenres = new List<Genres>()
            {
                Genres.Folk,
                Genres.Pop,
                Genres.Rap,
                Genres.Rock
            };

            if (!genres.Contains(Genres.All))
            {
                return genres;
            }

            return allGenres;

        }
        private IEnumerable<PlaylistItem> CreatePlaylistItems(ICollection<Song> filteredSongs, Playlist playlist)
        {
            foreach (var song in filteredSongs)
            {
                var item = new PlaylistItem()
                {
                    SongId = song.Id,
                    PlaylistId = playlist.Id,
                };
                this.context.PlaylistItem.Add(item);
                this.context.SaveChanges();

                yield return item;
            }

        }
        private IEnumerable<PlaylistItem> GetPlaylistItems(Guid playlistId)
        {
            var items = this.context.PlaylistItem.ToList();

            foreach (var item in items)
            {
                if (item.PlaylistId == playlistId)
                    yield return item;
            }

        }

        private ICollection<Song> Shuffle(List<Song> allSongs)
        {
            var random = new Random();

            int n = allSongs.Count;
            while (n > 1)
            {
                n--;
                int k = random.Next(n + 1);
                Song value = allSongs[k];
                allSongs[k] = allSongs[n];
                allSongs[n] = value;
            }

            return allSongs;
        }

    }
}
