﻿using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore.Internal;
using PlaylistGenerator.Data;
using PlaylistGenerator.Data.Models;
using PlaylistGenerator.Services.Contracts;
using PlaylistGenerator.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PlaylistGenerator.Services.Services
{
    public class RegisterService : IRegisterService
    {
        private readonly PlaylistGeneratorContext context;
        private readonly IMapper mapper;
        private readonly SignInManager<User> signInManager;
        private readonly UserManager<User> userManager;

        public RegisterService(PlaylistGeneratorContext context, IMapper mapper,
            SignInManager<User> signInManager, UserManager<User> userManager)
        {
            this.context = context;
            this.mapper = mapper;
            this.signInManager = signInManager;
            this.userManager = userManager;
        }

        public async Task<bool> RegisterUserAsync(UserDTO userDTO)
        {
            User user = this.mapper.Map<User>(userDTO);

            var result = await userManager.CreateAsync(user, userDTO.Password);

            if(result.Succeeded)
            {
                await this.userManager.AddToRoleAsync(user, "User");
               
                await this.signInManager.PasswordSignInAsync(user, userDTO.Password,
                                                                        isPersistent: true, lockoutOnFailure: false);

                await this.context.Users.AddAsync(user);
                this.context.Update(user);
                await this.context.SaveChangesAsync();
            }

            return result.Succeeded;
        }
    }
}
