﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using PlaylistGenerator.Data;
using PlaylistGenerator.Data.Models;
using PlaylistGenerator.Data.Seeding.JsonModels;
using PlaylistGenerator.Services.Contracts;
using PlaylistGenerator.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PlaylistGenerator.Services.Services
{
    public class UserService : IUserService
    {
        private readonly PlaylistGeneratorContext context;
        private readonly IMapper mapper;
        private readonly UserManager<User> userManager;

        public UserService(PlaylistGeneratorContext context, IMapper mapper, UserManager<User> userManager)
        {
            this.context = context;
            this.mapper = mapper;
            this.userManager = userManager;
        }

        public async Task<UserDTO> GetUserByUserNameAsync(string userName)
        {
            var user = await this.context.Users.FirstOrDefaultAsync(u => u.UserName == userName);
            var userDTO = this.mapper.Map<UserDTO>(user);

            return userDTO;
        }

        public async Task<bool> CreateUserAsync(UserDTO userDTO)
        {
            User user = mapper.Map<User>(userDTO);

            var result = await userManager.CreateAsync(user, userDTO.Password);

            if (result.Succeeded)
            {
                await this.userManager.AddToRoleAsync(user, "User");

                await context.Users.AddAsync(user);
                context.Update(user);
                await context.SaveChangesAsync();
            }

            return result.Succeeded;
        }

        public async Task<bool> DeleteUserAsync(Guid? id)
        {
            var user = await context.Users.FirstOrDefaultAsync(u => u.Id == id);

            if(user == null)
            {
                return false;
            }

            context.Users.Remove(user);
            await context.SaveChangesAsync();

            return true;
        }

        public async Task UpdateUserAsync(Guid id, string UserName, string FirstName, string LastName, string Email)
        {
            var user = await context.Users.FirstOrDefaultAsync(u => u.Id == id);

            if(FirstName!=null)
                user.FirstName = FirstName;
            if (LastName != null)
                user.LastName = LastName;
            if(UserName!=null)
                user.UserName = UserName;
            if(Email!=null)
                user.Email = Email;

            context.Update(user);
            await context.SaveChangesAsync();
        }

        public async Task<bool> BanUserAsync(Guid? id)
        {
            var user = await context.Users.FirstOrDefaultAsync(u => u.Id == id);

            if(user == null)
            {
                return false;
            }

            user.IsBanned = true;
            user.BanExpirationDate = DateTime.Now.AddDays(7);

            await context.SaveChangesAsync();

            return true;
        }

        public async Task<bool> UnbanUserAsync(Guid? id)
        {
            var user = await context.Users.FirstOrDefaultAsync(u => u.Id == id);

            if(user == null)
            {
                return false;
            }

            user.IsBanned = false;
            user.BanExpirationDate = null;

            await context.SaveChangesAsync();

            return true;
        }

        public ICollection<User> GetAllUsers()
        {
            var users = this.context.Users.Where(u => u.NormalizedUserName != null).ToArray();

            return users;
        }

        public async Task<UserDTO> GetUserById(Guid? id)
        {
            var user = await this.context.Users.FirstOrDefaultAsync(u => u.Id == id);

            var userDTO = this.mapper.Map<UserDTO>(user);

            return userDTO;
        }

        public async Task<bool> CreateUserAsync(UserDTO userDTO, string password)
        {
            User user = new User
            {
                Id = userDTO.Id,
                FirstName = userDTO.FirstName,
                LastName = userDTO.LastName,
                Email = userDTO.Email,
                UserName = userDTO.UserName
            };

            await context.Users.AddAsync(user);
            await context.SaveChangesAsync();

            return true;
        }
    }
}
