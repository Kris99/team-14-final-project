﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using PlaylistGenerator.Data;
using PlaylistGenerator.Data.Models;
using PlaylistGenerator.Services.Contracts;
using PlaylistGenerator.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlaylistGenerator.Services.Services
{
    public class SongService : ISongService
    {
        private readonly PlaylistGeneratorContext context;
        private readonly IMapper mapper;

        public SongService(PlaylistGeneratorContext context, IMapper mapper)
        {
            this.context = context;
            this.mapper = mapper;
        }

        public ICollection<Song> GetAllSongs()
        {
            return this.context.Songs.Take(500).ToList();
        }

        public async Task<SongDTO> GetSong(Guid id)
        {
            var song = await this.context.Songs.FirstOrDefaultAsync(s => s.Id == id);

            if (song == null)
                return null;

            var album = await this.context.Albums.FirstOrDefaultAsync(a => a.Id == song.AlbumId);
            var artist = await this.context.Artists.FirstOrDefaultAsync(a => a.Id == song.ArtistId);

            if(album == null || artist == null)
            {
                return null;
            }

            var dto = this.mapper.Map<SongDTO>(song);
            dto.Album = this.mapper.Map<AlbumDTO>(album);
            dto.Artist = this.mapper.Map<ArtistDTO>(artist);

            return dto;
        }
    }
}
