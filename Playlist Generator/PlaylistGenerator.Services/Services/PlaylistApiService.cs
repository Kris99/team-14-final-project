﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Migrations;
using PlaylistGenerator.Data;
using PlaylistGenerator.Data.Models;
using PlaylistGenerator.Data.Seeding.JsonModels;
using PlaylistGenerator.Services.Contracts;
using PlaylistGenerator.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace PlaylistGenerator.Services.Services
{
    public class PlaylistApiService : IPlaylistApiService
    {
        private readonly PlaylistGeneratorContext context;

        public PlaylistApiService(PlaylistGeneratorContext context)
        {
            this.context = context;
        }
        public async Task<IEnumerable<Playlist>> GetAllPlaylistsAsync()
        {
            var playlists = await this.context.Playlists.ToListAsync();

            return playlists;
        }

        public async Task<PlaylistCollection> GetPlaylistsByGenreAsync(string genre)
        {
            var url = $"https://api.deezer.com/search/playlist?q={genre}";
            var client = new HttpClient();

            var response = await client.GetAsync(url);
            var collectionJson = await response.Content.ReadAsStringAsync();
            var collection = JsonSerializer.Deserialize<PlaylistCollection>(collectionJson);

            return collection;
        }


        public async Task<SongCollection> GetPlaylistSongsAsync(long id)
        {
            var url = $"https://api.deezer.com/playlist/{id}/tracks";

            var client = new HttpClient();
            var response = await client.GetAsync(url);
            var songCollectionJson = await response.Content.ReadAsStringAsync();
            var songCollection = JsonSerializer.Deserialize<SongCollection>(songCollectionJson);

            return songCollection;
        }

        public async Task<JsonPlaylist> EditPlaylistAsync(long playlistId, string newTitle)
        {
            var url = $"https://api.deezer.com/playlist/{playlistId}";

            var client = new HttpClient();
            var response = await client.GetAsync(url);
            var playlist = await response.Content.ReadAsStringAsync();
            var jsonPlaylist = JsonSerializer.Deserialize<JsonPlaylist>(playlist);

            if(jsonPlaylist == null)
            {
                return null;
            }

            jsonPlaylist.Title = newTitle;

            return jsonPlaylist;
        }

        public async Task<JsonPlaylist> DeletePlaylistAsync(long playlistId)
        {
            var url = $"https://api.deezer.com/playlist/{playlistId}";

            var client = new HttpClient();
            var response = await client.GetAsync(url);
            var playlist = await response.Content.ReadAsStringAsync();
            var jsonPlaylist = JsonSerializer.Deserialize<JsonPlaylist>(playlist);

            return null;
        }
    }
}
