﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PlaylistGenerator.Data;
using PlaylistGenerator.Data.Seeding.JsonModels;
using PlaylistGenerator.Services.Contracts;
using PlaylistGenerator.Services.Services;
using System;
using System.Collections.Generic;
using System.Text;

namespace PlaylistGenerator.Services.Test.PlaylistApiServiceTests
{
    [TestClass]
    public class Delete_Should
    {
        [TestMethod]
        public void Delete_Should_Delete_ThePlaylist()
        {
            var options = Utils.GetOptions(nameof(Delete_Should_Delete_ThePlaylist));
            var service = new Mock<IPlaylistApiService>();

            using (var actContext = new PlaylistGeneratorContext(options))
            {
                var sut = new PlaylistApiService(actContext);
                var result = sut.DeletePlaylistAsync(1306931615);

                Assert.AreEqual(result.Result, null);
            }
        }
    }
}
