﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PlaylistGenerator.Data;
using PlaylistGenerator.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;
using PlaylistGenerator.Services.Services;
using Moq;
using PlaylistGenerator.Services.Contracts;
using PlaylistGenerator.Data.Seeding.JsonModels;
using System.Net.Http;
using System.Threading.Tasks;

namespace PlaylistGenerator.Services.Test.PlaylistApiServiceTests
{
    [TestClass]
    public class Get_Should
    {
        [TestMethod]
        public void GetAll_Should()
        {
            var options = Utils.GetOptions(nameof(GetAll_Should));
            var service = new Mock<IPlaylistApiService>();

            using (var actContext = new PlaylistGeneratorContext(options))
            {
                var sut = new PlaylistApiService(actContext);
                var result = sut.GetPlaylistsByGenreAsync("pop");


                Assert.IsInstanceOfType(result.Result, typeof(PlaylistCollection));
            }
        }

        [TestMethod]
        public void GetPlaylistSongs_Should()
        {
            var options = Utils.GetOptions(nameof(GetPlaylistSongs_Should));
            var service = new Mock<IPlaylistApiService>();

            using (var actContext = new PlaylistGeneratorContext(options))
            {
                var sut = new PlaylistApiService(actContext);
                var result = sut.GetPlaylistSongsAsync(1306931615);

                Assert.IsInstanceOfType(result.Result, typeof(SongCollection));
            }
        }

        [TestMethod]
        public async Task GetPlaylistByGenre_Should()
        {
            var options = Utils.GetOptions(nameof(GetPlaylistSongs_Should));
            var service = new Mock<IPlaylistApiService>();

            using (var actContext = new PlaylistGeneratorContext(options))
            {
                var sut = new PlaylistApiService(actContext);
                var result = await sut.GetPlaylistsByGenreAsync("Pop");

                Assert.IsInstanceOfType(result, typeof(PlaylistCollection));
            }
        }
    }
}
