﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PlaylistGenerator.Data;
using PlaylistGenerator.Data.Seeding.JsonModels;
using PlaylistGenerator.Services.Contracts;
using PlaylistGenerator.Services.Services;
using System;
using System.Collections.Generic;
using System.Text;

namespace PlaylistGenerator.Services.Test.PlaylistApiServiceTests
{
    [TestClass]
    public class Edit_Should
    {
        [TestMethod]
        public void Edit_Should_Update_ThePlaylist()
        {
            var options = Utils.GetOptions(nameof(Edit_Should_Update_ThePlaylist));
            var service = new Mock<IPlaylistApiService>();

            using (var actContext = new PlaylistGeneratorContext(options))
            {
                var sut = new PlaylistApiService(actContext);
                var result = sut.EditPlaylistAsync(1306931615, "The Boyz");

                Assert.AreEqual(result.Result.Title, "The Boyz");
            }
        }
    }
}
