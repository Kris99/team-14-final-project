﻿using Moq;
using PlaylistGenerator.Data.Models;
using PlaylistGenerator.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Text;

namespace PlaylistGenerator.Services.Test.TestConfigurations
{
    public class PlaylistServiceConfiguration : BaseTestSetup
    {
        protected Mock<ISongService> songService;
        protected User[] users;
        protected Album[] albums;
        protected Artist[] artists;
        protected Song[] songs;
        protected Playlist[] playlists;
        protected PlaylistItem[] items;

        protected override void AdditionalSetup()
        {
            songService = new Mock<ISongService>();


            users = new User[]
             {
                new User()
                {
                    Id = Guid.NewGuid(),
                    FirstName = "Kristian",
                    LastName = "Dimirtrov",
                    UserName = "Krisk0"
                },
                new User()
                {
                    Id = Guid.NewGuid(),
                    FirstName = "Georgi",
                    LastName = "Georgiev",
                    UserName = "Hellyz"
                },
             };
            albums = new Album[]
            {
                new Album()
                {
                    Id = Guid.NewGuid(),
                    Name = "Zlatnite momcheta",
                    Songs = new List<Song>()
                },
                new Album()
                {
                    Id = Guid.NewGuid(),
                    Name = "Red Pill Blues",
                    Songs = new List<Song>()
                },
                new Album()
                {
                    Id = Guid.NewGuid(),
                    Name = "ZLA10",
                    Songs = new List<Song>()
                },
                new Album()
                {
                    Id = Guid.NewGuid(),
                    Name = "My World",
                    Songs = new List<Song>()
                }
            };
            artists = new Artist[]
            {
                new Artist()
                {
                    Id = Guid.NewGuid(),
                    Name = "Justin Bieber",
                    IsDeleted = false,
                    Albums = new List<Album>()
                },
                new Artist()
                {
                    Id = Guid.NewGuid(),
                    Name = "Maroon 5",
                    IsDeleted = false,
                    Albums = new List<Album>()
                },
                new Artist()
                {
                    Id = Guid.NewGuid(),
                    Name = "100 kila",
                    IsDeleted = false,
                    Albums = new List<Album>()
                },
                new Artist()
                {
                    Id = Guid.NewGuid(),
                    Name = "Krisko",
                    IsDeleted = false,
                    Albums = new List<Album>()
                }
            };
            songs = new Song[]
            {
                new Song()
                {
                    Id = Guid.NewGuid(),
                    Title = "Baby",
                    AlbumId = albums[3].Id,
                    Album = albums[3],
                    ArtistId = artists[0].Id,
                    Artist = artists[0],
                    Duration = 305,
                    Genre = Genres.Pop,
                    Rank = 1,
                    Items = new List<PlaylistItem>()
                },
                new Song()
                {
                    Id = Guid.NewGuid(),
                    Title = "Babuli Jabulah",
                    AlbumId = albums[2].Id,
                    Album = albums[2],
                    ArtistId = artists[2].Id,
                    Artist = artists[2],
                    Duration = 315,
                    Genre = Genres.Rap,
                    Rank = 2,
                    Items = new List<PlaylistItem>()
                },
                new Song()
                {
                    Id = Guid.NewGuid(),
                    Title = "Zlatnite momcheta",
                    AlbumId = albums[0].Id,
                    Album = albums[0],
                    ArtistId = artists[3].Id,
                    Artist = artists[3],
                    Duration = 315,
                    Genre = Genres.Rap,
                    Rank = 3,
                    Items = new List<PlaylistItem>()
                },
                new Song()
                {
                    Id = Guid.NewGuid(),
                    Title = "Red Pill Blues",
                    AlbumId = albums[1].Id,
                    Album = albums[1],
                    ArtistId = artists[1].Id,
                    Artist = artists[1],
                    Duration = 410,
                    Genre = Genres.Rock,
                    Rank = 4,
                    Items = new List<PlaylistItem>()
                },
            };
            playlists = new Playlist[]
            {
                new Playlist
                {
                    Id = Guid.NewGuid(),
                    UserId = users[0].Id,
                    User = users[0],
                    Title = "BG muzika",
                    Items = new List<PlaylistItem>(),
                    AvrRank = 1,
                    TotalPlayTime = 165,
                    IsDeleted = false
                },
                new Playlist
                {
                    Id = Guid.NewGuid(),
                    UserId = users[1].Id,
                    User = users[1],
                    Title = "Summer hits",
                    Items = new List<PlaylistItem>(),
                    AvrRank = 2,
                    TotalPlayTime = 233,
                    IsDeleted = false
                },
                new Playlist
                {
                    Id = Guid.NewGuid(),
                    UserId = users[1].Id,
                    User = users[1],
                    Title = "Cool Mix",
                    Items = new List<PlaylistItem>(),
                    AvrRank = 3,
                    TotalPlayTime = 765,
                    IsDeleted = false
                }
            };
            items = new PlaylistItem[]
            {
                new PlaylistItem()
                {
                    SongId = songs[1].Id,
                    Song = songs[1],
                    PlaylistId = playlists[0].Id,
                    PLaylist = playlists[0]
                },
                new PlaylistItem()
                {
                    SongId = songs[2].Id,
                    Song = songs[2],
                    PlaylistId = playlists[0].Id,
                    PLaylist = playlists[0]
                },
                new PlaylistItem()
                {
                    SongId = songs[0].Id,
                    Song = songs[0],
                    PlaylistId = playlists[1].Id,
                    PLaylist = playlists[1]
                },
                new PlaylistItem()
                {
                    SongId = songs[3].Id,
                    Song = songs[3],
                    PlaylistId = playlists[1].Id,
                    PLaylist = playlists[1]
                },
                new PlaylistItem()
                {
                    SongId = songs[2].Id,
                    Song = songs[2],
                    PlaylistId = playlists[2].Id,
                    PLaylist = playlists[2]
                },
                new PlaylistItem()
                {
                    SongId = songs[3].Id,
                    Song = songs[3],
                    PlaylistId = playlists[2].Id,
                    PLaylist = playlists[2]
                },
            };

            playlists[0].Items.Add(items[0]);
            playlists[0].Items.Add(items[1]);
            playlists[1].Items.Add(items[2]);
            playlists[1].Items.Add(items[3]);
            playlists[2].Items.Add(items[4]);
            playlists[2].Items.Add(items[5]);

            albums[0].Songs.Add(songs[2]);
            albums[1].Songs.Add(songs[3]);
            albums[2].Songs.Add(songs[1]);
            albums[3].Songs.Add(songs[0]);

            artists[0].Albums.Add(albums[3]);
            artists[1].Albums.Add(albums[1]);
            artists[2].Albums.Add(albums[2]);
            artists[3].Albums.Add(albums[0]);
        }
    }
}
