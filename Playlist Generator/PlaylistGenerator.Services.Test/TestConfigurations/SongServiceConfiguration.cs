﻿using Moq;
using PlaylistGenerator.Data.Models;
using PlaylistGenerator.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Text;

namespace PlaylistGenerator.Services.Test.TestConfigurations
{
    public class SongServiceConfiguration : BaseTestSetup
    {
        protected Album[] albums;
        protected Artist[] artists;
        protected Song[] songs;

        protected override void AdditionalSetup()
        {
            albums = new Album[]
            {
                new Album()
                {
                    Id = Guid.NewGuid(),
                    Name = "Zlatnite momcheta",
                    Songs = new List<Song>()
                },
                new Album()
                {
                    Id = Guid.NewGuid(),
                    Name = "Red Pill Blues",
                    Songs = new List<Song>()
                },
                new Album()
                {
                    Id = Guid.NewGuid(),
                    Name = "ZLA10",
                    Songs = new List<Song>()
                },
                new Album()
                {
                    Id = Guid.NewGuid(),
                    Name = "My World",
                    Songs = new List<Song>()
                }
            };
            artists = new Artist[]
            {
                new Artist()
                {
                    Id = Guid.NewGuid(),
                    Name = "Justin Bieber",
                    IsDeleted = false,
                    Albums = new List<Album>()
                },
                new Artist()
                {
                    Id = Guid.NewGuid(),
                    Name = "Maroon 5",
                    IsDeleted = false,
                    Albums = new List<Album>()
                },
                new Artist()
                {
                    Id = Guid.NewGuid(),
                    Name = "100 kila",
                    IsDeleted = false,
                    Albums = new List<Album>()
                },
                new Artist()
                {
                    Id = Guid.NewGuid(),
                    Name = "Krisko",
                    IsDeleted = false,
                    Albums = new List<Album>()
                }
            };
            songs = new Song[]
            {
                new Song()
                {
                    Id = Guid.NewGuid(),
                    Title = "Baby",
                    AlbumId = albums[3].Id,
                    Album = albums[3],
                    ArtistId = artists[0].Id,
                    Artist = artists[0],
                    Duration = 305,
                    Genre = Genres.Pop,
                    Rank = 1,
                    Items = new List<PlaylistItem>()
                },
                new Song()
                {
                    Id = Guid.NewGuid(),
                    Title = "Babuli Jabulah",
                    AlbumId = albums[2].Id,
                    Album = albums[2],
                    ArtistId = artists[2].Id,
                    Artist = artists[2],
                    Duration = 315,
                    Genre = Genres.Rap,
                    Rank = 2,
                    Items = new List<PlaylistItem>()
                },
                new Song()
                {
                    Id = Guid.NewGuid(),
                    Title = "Zlatnite momcheta",
                    AlbumId = albums[0].Id,
                    Album = albums[0],
                    ArtistId = artists[3].Id,
                    Artist = artists[3],
                    Duration = 315,
                    Genre = Genres.Rap,
                    Rank = 3,
                    Items = new List<PlaylistItem>()
                },
                new Song()
                {
                    Id = Guid.NewGuid(),
                    Title = "Red Pill Blues",
                    AlbumId = albums[1].Id,
                    Album = albums[1],
                    ArtistId = artists[1].Id,
                    Artist = artists[1],
                    Duration = 410,
                    Genre = Genres.Rock,
                    Rank = 4,
                    Items = new List<PlaylistItem>()
                },
            };

            albums[0].Songs.Add(songs[2]);
            albums[1].Songs.Add(songs[3]);
            albums[2].Songs.Add(songs[1]);
            albums[3].Songs.Add(songs[0]);

            artists[0].Albums.Add(albums[3]);
            artists[1].Albums.Add(albums[1]);
            artists[2].Albums.Add(albums[2]);
            artists[3].Albums.Add(albums[0]);
        }
    }
}
