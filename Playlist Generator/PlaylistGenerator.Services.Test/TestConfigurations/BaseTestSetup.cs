﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PlaylistGenerator.Data;
using System;
using System.Reflection;

namespace PlaylistGenerator.Services.Test.TestConfigurations
{
    [TestClass]
    public abstract class BaseTestSetup
    {
        protected PlaylistGeneratorContext context;
        protected IMapper mapper;

        private MapperConfiguration config = new MapperConfiguration(a =>
        {
            a.AddMaps(Assembly.GetAssembly(typeof(Playlist_Generator.Mapper.Mapper)));
        });

        [TestInitialize]
        public void Init()
        {
            var options = GetOptions(Guid.NewGuid().ToString());

            context = new PlaylistGeneratorContext(options);

            mapper = config.CreateMapper();

            this.AdditionalSetup();
        }

        protected abstract void AdditionalSetup();

        private DbContextOptions<PlaylistGeneratorContext> GetOptions(string databaseName)
        {
            return new DbContextOptionsBuilder<PlaylistGeneratorContext>()
                .UseInMemoryDatabase(databaseName)
                .Options;
        }
    }
}
