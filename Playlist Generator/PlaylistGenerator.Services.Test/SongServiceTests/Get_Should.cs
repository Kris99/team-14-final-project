﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PlaylistGenerator.Data.Models;
using PlaylistGenerator.Services.Services;
using PlaylistGenerator.Services.Test.TestConfigurations;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PlaylistGenerator.Services.Test.SongServiceTests
{
    [TestClass]
    public class Get_Should : SongServiceConfiguration
    {
        [TestMethod]
        public void GetAllSongs_Should_ReturnAllSongs()
        {
            context.AddRange(songs);
            context.SaveChanges();

            var sut = new SongService(context, mapper);
            var result = sut.GetAllSongs();

            Assert.AreEqual(result.Count, 4);
            Assert.IsInstanceOfType(result, typeof(ICollection<Song>));
        }

        [TestMethod]
        public async Task GetSong_Should_Return_TheSong()
        {
            context.AddRange(songs);
            context.AddRange(albums);
            context.AddRange(artists);
            context.SaveChanges();

            var song = songs[0];

            var sut = new SongService(context, mapper);
            var result = await sut.GetSong(song.Id);

            Assert.AreEqual(result.Title, song.Title);
            Assert.AreEqual(result.Album.Name, song.Album.Name);
            Assert.AreEqual(result.Artist.Name, song.Artist.Name);
            Assert.AreEqual(result.Duration, song.Duration);
        }

        [TestMethod]
        public async Task GetSong_Should_Return_Null_WhenWrongIdisGiven()
        {
            context.AddRange(songs);
            context.AddRange(albums);
            context.AddRange(artists);
            context.SaveChanges();

            Guid id = Guid.NewGuid();

            var sut = new SongService(context, mapper);
            var result = await sut.GetSong(id);

            Assert.IsNull(result);
        }
    }
}
