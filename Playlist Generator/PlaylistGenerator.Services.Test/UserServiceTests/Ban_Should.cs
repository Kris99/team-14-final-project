﻿using AutoMapper;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PlaylistGenerator.Data;
using PlaylistGenerator.Data.Models;
using PlaylistGenerator.Services.Contracts;
using PlaylistGenerator.Services.DTOs;
using PlaylistGenerator.Services.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlaylistGenerator.Services.Test.UserServiceTests
{
    [TestClass]
    public class Ban_Should
    {
        [TestMethod]
        public async Task Ban_Should_ReturnTrue_And_BanTheUser()
        {
            var options = Utils.GetOptions(nameof(Ban_Should_ReturnTrue_And_BanTheUser));
            var service = new Mock<IUserService>();
            var mapper = new Mock<IMapper>();

            var user = new User
            {
                Id = Guid.NewGuid(),
                FirstName = "Kristian",
                LastName = "Dimitrov",
                Email = "kris.dimitrov99@gmail.com",
                UserName = "Krisko",
                IsBanned = false
            };

            using (var actContext = new PlaylistGeneratorContext(options))
            {
                actContext.Users.Add(user);
                actContext.SaveChanges();
            }

            //Act
            using (var arrangeContext = new PlaylistGeneratorContext(options))
            {
                var sut = new UserService(arrangeContext, mapper.Object, default);
                var result = await sut.BanUserAsync(user.Id);
                var bannedUser = arrangeContext.Users.First(u => u.Id == user.Id);

                //Assert
                Assert.IsTrue(result);
                Assert.IsTrue(bannedUser.IsBanned);
            }
        }


        [TestMethod]
        public async Task Ban_Should_ReturnFalse_And_NotBanTheUser()
        {
            var options = Utils.GetOptions(nameof(Ban_Should_ReturnTrue_And_BanTheUser));
            var mapper = new Mock<IMapper>();

            var user = new User
            {
                Id = Guid.NewGuid(),
                FirstName = "Kristian",
                LastName = "Dimitrov",
                Email = "kris.dimitrov99@gmail.com",
                UserName = "Krisko",
                IsBanned = false
            };

            using (var actContext = new PlaylistGeneratorContext(options))
            {
                actContext.Users.Add(user);
                actContext.SaveChanges();
            }

            //Act
            using (var arrangeContext = new PlaylistGeneratorContext(options))
            {
                var sut = new UserService(arrangeContext, mapper.Object, default);
                var result = await sut.BanUserAsync(Guid.NewGuid());
                var bannedUser = arrangeContext.Users.First(u => u.Id == user.Id);

                //Assert
                Assert.IsFalse(result);
                Assert.IsFalse(bannedUser.IsBanned);
            }
        }
    }
}
