﻿using AutoMapper;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PlaylistGenerator.Data;
using PlaylistGenerator.Data.Models;
using PlaylistGenerator.Services.Contracts;
using PlaylistGenerator.Services.DTOs;
using PlaylistGenerator.Services.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace PlaylistGenerator.Services.Test.UserServiceTests
{
    [TestClass]
    public class Create_Should
    {
        [TestMethod]
        public void Create_Should_InsertUser()
        {
            var options = Utils.GetOptions(nameof(Create_Should_InsertUser));
            MapperConfiguration config = new MapperConfiguration(a =>
            {
                a.AddMaps(Assembly.GetAssembly(typeof(Playlist_Generator.Mapper.Mapper)));
            });

            IMapper mapper = config.CreateMapper();

            var user = new User
            {
                Id = Guid.NewGuid(),
                FirstName = "Kristian",
                LastName = "Dimitrov",
                Email = "kris.dimitrov99@gmail.com",
                UserName = "Krisko"
            };

            var user2 = new User
            {
                Id = Guid.NewGuid(),
                FirstName = "Mario",
                LastName = "Dimitrov",
                Email = "mariok1996@abv.bg",
                UserName = "Mariochkata"
            };

            //Act
            using (var actContext = new PlaylistGeneratorContext(options))
            {
                var sut = new UserService(actContext, mapper, default);
                var userDTO = new UserDTO(user);
                var result = sut.CreateUserAsync(userDTO, "080999km");

                //Assert
                Assert.IsInstanceOfType(result, typeof(Task<bool>));
                Assert.AreEqual(actContext.Users.Count(), 1);
                Assert.AreEqual(user.Id, actContext.Users.First().Id);
                Assert.AreEqual(user.FirstName, actContext.Users.First().FirstName);
                Assert.AreEqual(user.LastName, actContext.Users.First().LastName);
                Assert.AreEqual(user.Email, actContext.Users.First().Email);
                Assert.AreEqual(user.UserName, actContext.Users.First().UserName);
            }
        }
    }
}
