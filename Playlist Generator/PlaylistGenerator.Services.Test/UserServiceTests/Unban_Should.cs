﻿using AutoMapper;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PlaylistGenerator.Data;
using PlaylistGenerator.Data.Models;
using PlaylistGenerator.Services.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlaylistGenerator.Services.Test.UserServiceTests
{
    [TestClass]
    public class Unban_Should
    {
        [TestMethod]
        public async Task Unban_Should_ReturnTrue_And_UnbanTheUser()
        {
            var options = Utils.GetOptions(nameof(Unban_Should_ReturnTrue_And_UnbanTheUser));
            var mapper = new Mock<IMapper>();

            var user = new User
            {
                Id = Guid.NewGuid(),
                FirstName = "Kristian",
                LastName = "Dimitrov",
                Email = "kris.dimitrov99@gmail.com",
                UserName = "Krisko",
                IsBanned = true
            };

            using (var actContext = new PlaylistGeneratorContext(options))
            {
                actContext.Users.Add(user);
                actContext.SaveChanges();
            }

            //Act
            using (var arrangeContext = new PlaylistGeneratorContext(options))
            {
                var sut = new UserService(arrangeContext, mapper.Object, default);
                var result = await sut.UnbanUserAsync(user.Id);
                var bannedUser = arrangeContext.Users.First(u => u.Id == user.Id);

                //Assert
                Assert.IsTrue(result);
                Assert.IsFalse(bannedUser.IsBanned);
            }
        }


        [TestMethod]
        public async Task Unban_Should_ReturnFalse()
        {
            var options = Utils.GetOptions(nameof(Unban_Should_ReturnFalse));
            var mapper = new Mock<IMapper>();

            var user = new User
            {
                Id = Guid.NewGuid(),
                FirstName = "Kristian",
                LastName = "Dimitrov",
                Email = "kris.dimitrov99@gmail.com",
                UserName = "Krisko",
                IsBanned = true
            };

            using (var actContext = new PlaylistGeneratorContext(options))
            {
                actContext.Users.Add(user);
                actContext.SaveChanges();
            }

            //Act
            using (var arrangeContext = new PlaylistGeneratorContext(options))
            {
                var sut = new UserService(arrangeContext, mapper.Object, default);
                var result = await sut.UnbanUserAsync(Guid.NewGuid());
                var bannedUser = arrangeContext.Users.First(u => u.Id == user.Id);

                //Assert
                Assert.IsFalse(result);
                Assert.IsTrue(bannedUser.IsBanned);
            }
        }
    }
}
