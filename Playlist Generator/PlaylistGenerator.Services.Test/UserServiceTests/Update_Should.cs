﻿using AutoMapper;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PlaylistGenerator.Data;
using PlaylistGenerator.Data.Models;
using PlaylistGenerator.Services.Contracts;
using PlaylistGenerator.Services.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace PlaylistGenerator.Services.Test.UserServiceTests
{
    [TestClass]
    public class Update_Should
    {
        [TestMethod]
        public void Update_Should_NotChangeAnything()
        {
            var options = Utils.GetOptions(nameof(Update_Should_NotChangeAnything));
            MapperConfiguration config = new MapperConfiguration(a =>
            {
                a.AddMaps(Assembly.GetAssembly(typeof(Playlist_Generator.Mapper.Mapper)));
            });

            IMapper mapper = config.CreateMapper();

            var user = new User
            {
                Id = Guid.NewGuid(),
                FirstName = "Kristian",
                LastName = "Dimitrov",
                Email = "kris.dimitrov99@gmail.com",
                UserName = "Krisko"
            };

            var user2 = new User
            {
                Id = Guid.NewGuid(),
                FirstName = "Mario",
                LastName = "Dimitrov",
                Email = "mariok1996@abv.bg",
                UserName = "Mariochkata"
            };

            using (var arrangeContext = new PlaylistGeneratorContext(options))
            {
                arrangeContext.Users.Add(user);
                arrangeContext.SaveChanges();
            }

            //Act
            using (var actContext = new PlaylistGeneratorContext(options))
            {
                var sut = new UserService(actContext, mapper, default);
                var result = sut.UpdateUserAsync(Guid.NewGuid(), user.UserName, user.FirstName, user.LastName, user.Email);
                var updatedUser = actContext.Users.FirstOrDefault(u => u.Id == user.Id);

                //Assert
                Assert.AreEqual(updatedUser.FirstName, user.FirstName);
            }
        }

        [TestMethod]
        public void Update_Should_UpdateTheUser()
        {
            var options = Utils.GetOptions(nameof(Update_Should_UpdateTheUser));
            MapperConfiguration config = new MapperConfiguration(a =>
            {
                a.AddMaps(Assembly.GetAssembly(typeof(Playlist_Generator.Mapper.Mapper)));
            });

            IMapper mapper = config.CreateMapper();

            var user = new User
            {
                Id = Guid.NewGuid(),
                FirstName = "Kristian",
                LastName = "Dimitrov",
                Email = "kris.dimitrov99@gmail.com",
                UserName = "Krisko"
            };

            var user2 = new User
            {
                Id = Guid.NewGuid(),
                FirstName = "Mario",
                LastName = "Dimitrov",
                Email = "mariok1996@abv.bg",
                UserName = "Mariochkata"
            };

            using (var arrangeContext = new PlaylistGeneratorContext(options))
            {
                arrangeContext.Users.Add(user);
                arrangeContext.SaveChanges();
            }

            //Act
            using (var actContext = new PlaylistGeneratorContext(options))
            {
                var sut = new UserService(actContext, mapper, default);
                var result = sut.UpdateUserAsync(user.Id, "New First Name", "New Last Name", "New UserName", "New Email");
                var updatedUser = actContext.Users.FirstOrDefault(u => u.Id == user.Id);

                //Assert
                Assert.AreNotEqual(updatedUser.FirstName, user.FirstName);
                Assert.AreNotEqual(updatedUser.LastName, user.LastName);
                Assert.AreNotEqual(updatedUser.UserName, user.UserName);
                Assert.AreNotEqual(updatedUser.Email, user.Email);
            }
        }
    }
}
