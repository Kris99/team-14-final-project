﻿using AutoMapper;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PlaylistGenerator.Data;
using PlaylistGenerator.Data.Models;
using PlaylistGenerator.Services.Contracts;
using PlaylistGenerator.Services.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace PlaylistGenerator.Services.Test.UserServiceTests
{
    [TestClass]
    public class Get_Should
    {
        [TestMethod]
        public void GetAllUsers_Should_Return_AllUsers()
        {
            var options = Utils.GetOptions(nameof(GetAllUsers_Should_Return_AllUsers));
            var mapper = new Mock<IMapper>();

            var user = new User
            {
                Id = Guid.NewGuid(),
                FirstName = "Kristian",
                LastName = "Dimitrov",
                Email = "kris.dimitrov@gmail.com",
                UserName = "Krisko",
                NormalizedUserName = "KRISKO"
            };

            var user2 = new User
            {
                Id = Guid.NewGuid(),
                FirstName = "Mario",
                LastName = "Dimitrov",
                Email = "mariok1996@abv.bg",
                UserName = "Mariochkata",
                NormalizedUserName = "MARIOCHKATA"
            };

            using (var arrangeContext = new PlaylistGeneratorContext(options))
            {
                arrangeContext.Users.Add(user);
                arrangeContext.Users.Add(user2);
                arrangeContext.SaveChanges();
            }

            //Act
            using (var actContext = new PlaylistGeneratorContext(options))
            {
                var sut = new UserService(actContext, mapper.Object, default);
                var result = sut.GetAllUsers();

                //Assert
                Assert.AreEqual(result.First().Id, user.Id);
                Assert.AreEqual(result.Last().Id, user2.Id);
                Assert.AreEqual(result.First().FirstName, user.FirstName);
                Assert.AreEqual(result.Last().LastName, user2.LastName);
            }
        }
        [TestMethod]
        public async Task GetUserById_Should_Return_TheUser()
        {
            var options = Utils.GetOptions(nameof(GetUserById_Should_Return_TheUser));
            MapperConfiguration config = new MapperConfiguration(a =>
            {
                a.AddMaps(Assembly.GetAssembly(typeof(Playlist_Generator.Mapper.Mapper)));
            });

            IMapper mapper = config.CreateMapper();

            var user = new User
            {
                Id = Guid.NewGuid(),
                FirstName = "Kristian",
                LastName = "Dimitrov",
                Email = "kris.dimitrov@gmail.com",
                UserName = "Krisko"
            };

            var user2 = new User
            {
                Id = Guid.NewGuid(),
                FirstName = "Mario",
                LastName = "Dimitrov",
                Email = "mariok1996@abv.bg",
                UserName = "Mariochkata"
            };

            using (var arrangeContext = new PlaylistGeneratorContext(options))
            {
                arrangeContext.Users.Add(user);
                arrangeContext.Users.Add(user2);
                arrangeContext.SaveChanges();
            }

            //Act
            using (var actContext = new PlaylistGeneratorContext(options))
            {
                var sut = new UserService(actContext, mapper, default);
                var result = await sut.GetUserById(user.Id);

                //Assert
                Assert.AreEqual(result.FirstName, user.FirstName);
                Assert.AreEqual(result.LastName, user.LastName);
            }
        }
        [TestMethod]
        public async Task GetUserById_Should_Return_Null()
        {
            var options = Utils.GetOptions(nameof(GetUserById_Should_Return_Null));
            MapperConfiguration config = new MapperConfiguration(a =>
            {
                a.AddMaps(Assembly.GetAssembly(typeof(Playlist_Generator.Mapper.Mapper)));
            });

            IMapper mapper = config.CreateMapper();

            var user = new User
            {
                Id = Guid.NewGuid(),
                FirstName = "Kristian",
                LastName = "Dimitrov",
                Email = "kris.dimitrov@gmail.com",
                UserName = "Krisko"
            };

            var user2 = new User
            {
                Id = Guid.NewGuid(),
                FirstName = "Mario",
                LastName = "Dimitrov",
                Email = "mariok1996@abv.bg",
                UserName = "Mariochkata"
            };

            using (var arrangeContext = new PlaylistGeneratorContext(options))
            {
                arrangeContext.Users.Add(user);
                arrangeContext.Users.Add(user2);
                arrangeContext.SaveChanges();
            }

            //Act
            using (var actContext = new PlaylistGeneratorContext(options))
            {
                var sut = new UserService(actContext, mapper, default);
                var result = await sut.GetUserById(Guid.NewGuid());

                //Assert
                Assert.IsNull(result);
            }
        }

        [TestMethod]
        public async Task GetUserByUserName_Should_ReturnNull()
        {
            var options = Utils.GetOptions(nameof(GetUserByUserName_Should_ReturnNull));
            MapperConfiguration config = new MapperConfiguration(a =>
            {
                a.AddMaps(Assembly.GetAssembly(typeof(Playlist_Generator.Mapper.Mapper)));
            });

            IMapper mapper = config.CreateMapper();

            var user = new User
            {
                Id = Guid.NewGuid(),
                FirstName = "Kristian",
                LastName = "Dimitrov",
                Email = "kris.dimitrov@gmail.com",
                UserName = "Krisko"
            };

            var user2 = new User
            {
                Id = Guid.NewGuid(),
                FirstName = "Mario",
                LastName = "Dimitrov",
                Email = "mariok1996@abv.bg",
                UserName = "Mariochkata"
            };

            using (var arrangeContext = new PlaylistGeneratorContext(options))
            {
                arrangeContext.Users.Add(user);
                arrangeContext.Users.Add(user2);
                arrangeContext.SaveChanges();
            }

            //Act
            using (var actContext = new PlaylistGeneratorContext(options))
            {
                var sut = new UserService(actContext, mapper, default);
                var result = await sut.GetUserByUserNameAsync("Hellyz");

                //Assert
                Assert.IsNull(result);
            }
        }

        [TestMethod]
        public async Task GetUserByUserName_Should_ReturnTheUser()
        {
            var options = Utils.GetOptions(nameof(GetUserByUserName_Should_ReturnTheUser));
            MapperConfiguration config = new MapperConfiguration(a =>
            {
                a.AddMaps(Assembly.GetAssembly(typeof(Playlist_Generator.Mapper.Mapper)));
            });

            IMapper mapper = config.CreateMapper();

            var user = new User
            {
                Id = Guid.NewGuid(),
                FirstName = "Kristian",
                LastName = "Dimitrov",
                Email = "kris.dimitrov@gmail.com",
                UserName = "Krisko"
            };
            var user2 = new User
            {
                Id = Guid.NewGuid(),
                FirstName = "Mario",
                LastName = "Dimitrov",
                Email = "mariok1996@abv.bg",
                UserName = "Mariochkata"
            };

            using (var arrangeContext = new PlaylistGeneratorContext(options))
            {
                arrangeContext.Users.Add(user);
                arrangeContext.Users.Add(user2);
                arrangeContext.SaveChanges();
            }

            //Act
            using (var actContext = new PlaylistGeneratorContext(options))
            {
                var sut = new UserService(actContext, mapper, default);
                var result = await sut.GetUserByUserNameAsync(user.UserName);

                //Assert
                Assert.AreEqual(result.FirstName, user.FirstName);
                Assert.AreEqual(result.LastName, user.LastName);
            }
        }


    }
}
