﻿using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PlaylistGenerator.Data;
using PlaylistGenerator.Data.Models;
using PlaylistGenerator.Services.Contracts;
using PlaylistGenerator.Services.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlaylistGenerator.Services.Test.UserServiceTests
{
    [TestClass]
    public class Delete_Should
    {
        [TestMethod]
        public void Delete_Should_RemoveUser()
        {
            var options = Utils.GetOptions(nameof(Delete_Should_RemoveUser));
            var service = new Mock<IUserService>();
            var mapper = new Mock<IMapper>();

            var user = new User
            {
                Id = Guid.NewGuid(),
                FirstName = "Kristian",
                LastName = "Dimitrov",
                Email = "kris.dimitrov99@gmail.com",
                UserName = "Krisko"
            };

            var user2 = new User
            {
                Id = Guid.NewGuid(),
                FirstName = "Mario",
                LastName = "Dimitrov",
                Email = "mariok1996@abv.bg",
                UserName = "Mariochkata"
            };

            using (var arrangeContext = new PlaylistGeneratorContext(options))
            {
                arrangeContext.Users.Add(user);
                arrangeContext.Users.Add(user2);
                arrangeContext.SaveChanges();
            }

            //Act
            using (var actContext = new PlaylistGeneratorContext(options))
            {
                var sut = new UserService(actContext, mapper.Object, default);
                var result = sut.DeleteUserAsync(user.Id);

                //Assert
                Assert.AreEqual(actContext.Users.Count(), 1);
            }
        }

        [TestMethod]
        public void Delete_Should_ReturnFalse()
        {
            var options = Utils.GetOptions(nameof(Delete_Should_ReturnFalse));
            var service = new Mock<IUserService>();
            var mapper = new Mock<IMapper>();

            var user = new User
            {
                Id = Guid.NewGuid(),
                FirstName = "Kristian",
                LastName = "Dimitrov",
                Email = "kris.dimitrov99@gmail.com",
                UserName = "Krisko"
            };

            var user2 = new User
            {
                Id = Guid.NewGuid(),
                FirstName = "Mario",
                LastName = "Dimitrov",
                Email = "mariok1996@abv.bg",
                UserName = "Mariochkata"
            };

            using (var arrangeContext = new PlaylistGeneratorContext(options))
            {
                arrangeContext.Users.Add(user);
                arrangeContext.Users.Add(user2);
                arrangeContext.SaveChanges();
            }

            //Act
            using (var actContext = new PlaylistGeneratorContext(options))
            {
                var sut = new UserService(actContext, mapper.Object, default);
                var result = sut.DeleteUserAsync(Guid.NewGuid());

                //Assert

                Assert.AreEqual(actContext.Users.Count(), 2);
                Assert.AreEqual(result.Result, false);
            }
        }
    }
}
