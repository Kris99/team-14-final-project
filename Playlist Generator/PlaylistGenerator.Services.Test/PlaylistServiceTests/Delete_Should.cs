﻿using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PlaylistGenerator.Services.Services;
using PlaylistGenerator.Services.Test.TestConfigurations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlaylistGenerator.Services.Test.PlaylistServiceTests
{
    [TestClass]
    public class Delete_Should : PlaylistServiceConfiguration
    {
        [TestMethod]
        public async Task Delete_Should_Return_True()
        {
            context.AddRange(playlists);
            context.SaveChanges();

            Guid id = playlists[0].Id;

            var sut = new PlaylistService(context, songService.Object, mapper);
            var result = await sut.DeletePlaylist(id);
            var playlistsLeft = await context.Playlists.Select(pl => pl.IsDeleted == false).ToListAsync();

            Assert.IsTrue(result);
            Assert.AreEqual(playlistsLeft.Count(), 3);
            
        }

        [TestMethod]
        public async Task Delete_Should_Return_False()
        {
            context.AddRange(playlists);
            context.SaveChanges();

            Guid id = Guid.NewGuid();

            var sut = new PlaylistService(context, songService.Object, mapper);
            var result = await sut.DeletePlaylist(id);

            Assert.IsFalse(result);
        }
    }
}
