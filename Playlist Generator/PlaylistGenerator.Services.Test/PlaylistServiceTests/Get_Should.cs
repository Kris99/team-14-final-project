﻿using AutoMapper;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PlaylistGenerator.Data;
using PlaylistGenerator.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Text;
using PlaylistGenerator.Services.Services;
using System.Linq;
using PlaylistGenerator.Data.Models;
using PlaylistGenerator.Services.DTOs;
using Playlist_Generator.Mapper;
using PlaylistGenerator.Services.Test.TestConfigurations;
using System.Threading.Tasks;

namespace PlaylistGenerator.Services.Test.PlaylistServiceTests
{
    [TestClass]
    public class Get_Should : PlaylistServiceConfiguration
    {
        [TestMethod]
        public void GetPlaylists_Should_Return_Collection()
        {
            //Act
            context.AddRange(playlists);
            context.SaveChanges();

            var sut = new PlaylistService(context, songService.Object, mapper);
            var result = sut.GetAllPlaylists();

            //Arrange
            Assert.AreEqual(result.Count(), 3);
        }

        [TestMethod]
        public async Task GetPlaylist_Should_Return_Playlist()
        {
            //Act
            context.AddRange(playlists);
            context.SaveChanges();

            var playlist = playlists[0];

            var sut = new PlaylistService(context, songService.Object, mapper);
            var result = await sut.GetPlaylist(playlist.Id);

            //Arrange
            Assert.AreEqual(playlist.Title, result.Title);
            Assert.AreEqual(playlist.Id, result.Id);
           
        }

        [TestMethod]
        public async Task GetPlaylist_Should_Return_Null()
        {
            //Act
            context.AddRange(playlists);
            context.SaveChanges();

            var playlist = new Playlist()
            {
                Id = Guid.NewGuid()
            };

            var sut = new PlaylistService(context, songService.Object, mapper);
            var result = await sut.GetPlaylist(playlist.Id);

            //Arrange
            Assert.IsNull(result);

        }

        [TestMethod]
        public void GetTopPlaylists_Should_Return_TopPlaylists()
        {
            //Act
            context.AddRange(playlists);
            context.SaveChanges();

            var playlist = new Playlist()
            {
                Id = Guid.NewGuid(),
                Title = "Greatest Hits",
                AvrRank = 4 //lowest rank
            };

            context.Add(playlist);
            context.SaveChanges();

            var sut = new PlaylistService(context, songService.Object, mapper);
            var result = sut.GetTop3Playlists().ToList();  //the last playlist we have added to the context shouldn't be added to this collection

            //Arrange
            Assert.IsFalse(result.Any(pl => pl.Title == playlist.Title));
            Assert.IsTrue(result.Any(pl => pl.Title == playlists[0].Title));

        }



    }
}
