﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PlaylistGenerator.Data.Models;
using PlaylistGenerator.Services.Services;
using PlaylistGenerator.Services.Test.TestConfigurations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlaylistGenerator.Services.Test.PlaylistServiceTests
{
    [TestClass]
    public class Generate_Should : PlaylistServiceConfiguration
    {
        [TestMethod]
        public async Task GeneratePlaylist_WithAllGenres_Should_ReturnThePlaylist()
        {
            //Act
            context.AddRange(songs);
            context.AddRange(users);          
            context.SaveChanges();

            var genrePercentages = new Dictionary<Genres, double>() { { Genres.All, 100.00 } };

            var songService = new SongService(context, mapper);
            var sut = new PlaylistService(context, songService, mapper);
            var playlist = await sut.GeneratePlaylistAsync(800, true, true, genrePercentages, users[0], "Never Gonna Give Up");

            //Assert
            Assert.AreEqual(playlist.Title, "Never Gonna Give Up");
            Assert.AreEqual(context.Playlists.Count(), 1);
            Assert.AreEqual(context.PlaylistItem.Count(), 4);
        }


        [TestMethod]
        [DataRow(Genres.Rock, 50.00, Genres.Pop, 50.00)]
        [DataRow(Genres.Rap, 30.00, Genres.Pop, 70.00)]
        [DataRow(Genres.Rock, 20.00, Genres.Rap, 80.00)]
        public async Task GeneratePlaylist_WithSomeGenres_Should_ReturnThePlaylist(Genres genre1, double percent1, Genres genre2, double percent2)
        {
            //Act
            context.AddRange(songs);
            context.AddRange(users);
            context.SaveChanges();

            var genrePercentages = new Dictionary<Genres, double>()
            {
                {genre1, percent1 },
                {genre2, percent2 }
            };

            var songService = new SongService(context, mapper);
            var sut = new PlaylistService(context, songService, mapper);
            var playlist = await sut.GeneratePlaylistAsync(800, true, true, genrePercentages, users[0], "Never Gonna Give Up");

            //Assert
            Assert.AreEqual(playlist.Title, "Never Gonna Give Up");
            Assert.AreEqual(context.Playlists.Count(), 1);
        }
    }
}
