﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PlaylistGenerator.Services.DTOs;
using PlaylistGenerator.Services.Services;
using PlaylistGenerator.Services.Test.TestConfigurations;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PlaylistGenerator.Services.Test.PlaylistServiceTests
{
    [TestClass]
    public class Edit_Should : PlaylistServiceConfiguration
    {
        [TestMethod]
        public async Task EditPlaylist_Should_Return_EditedPlaylist()
        {
            context.AddRange(playlists);
            context.AddRange(users);
            context.SaveChanges();

            var playlistDto = mapper.Map<PlaylistDTO>(playlists[1]);
            playlistDto.Title = "Fitness Motivation";

            var sut = new PlaylistService(context, songService.Object, mapper);
            var result = await sut.EditPlaylist(playlistDto);

            playlistDto.Title = "Top Hits";

            Assert.AreNotEqual(playlistDto.Title, result.Title);
            Assert.AreEqual(result.Title, "Fitness Motivation");
        }

        [TestMethod]
        public async Task EditPlaylist_Should_Return_Null()
        {
            context.AddRange(playlists);
            context.AddRange(users);
            context.SaveChanges();

            var playlistDto = mapper.Map<PlaylistDTO>(playlists[1]);
            playlistDto.Id = Guid.NewGuid();
            playlistDto.Title = "Fitness Motivation";

            var sut = new PlaylistService(context, songService.Object, mapper);
            var result = await sut.EditPlaylist(playlistDto);

            playlistDto.Title = "Top Hits";

            Assert.IsNull(result);
        }
    }
}
