using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using PlaylistGenerator.Data;
using PlaylistGenerator.Data.Models;

namespace PlaylistGenerator.Services.Test
{

    public class Utils
    {
        public static DbContextOptions<PlaylistGeneratorContext> GetOptions(string databaseName)
        {
            return new DbContextOptionsBuilder<PlaylistGeneratorContext>()
                .UseInMemoryDatabase(databaseName)
                .Options;
        }
        
    }
}
