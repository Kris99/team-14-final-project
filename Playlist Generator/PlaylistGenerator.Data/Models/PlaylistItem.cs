﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PlaylistGenerator.Data.Models
{
    public class PlaylistItem
    {
        public Guid SongId { get; set; }
        public Song Song { get; set; }
        public Guid PlaylistId { get; set; }
        public Playlist PLaylist { get; set; }
    }
}
