﻿using PlaylistGenerator.Data.Models.Abstract;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace PlaylistGenerator.Data.Models
{
    public class Playlist : Entity
    {
        [Key]
        public Guid Id { get; set; }
        [Required]
        public string Title { get; set; }
        public string URL { get; set; }
        public string Genres { get; set; }
        [Required]
        public int TotalPlayTime { get; set; }
        [Required]
        public double AvrRank { get; set; }
        
        public ICollection<PlaylistItem> Items { get; set; }

        public Guid UserId { get; set; }

        public User User { get; set; }
    }
}