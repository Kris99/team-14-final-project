﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PlaylistGenerator.Data.Models
{
    public enum Genres
    {
        All,
        Pop,
        Rock,
        Rap,
        Folk
    }
}
