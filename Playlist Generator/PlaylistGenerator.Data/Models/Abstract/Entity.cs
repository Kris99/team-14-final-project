﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Text.Json.Serialization;

namespace PlaylistGenerator.Data.Models.Abstract
{
    public abstract class Entity
    {
        [DisplayName("Added on")]
        [JsonIgnore]
        public DateTime AddedOn { get; set; } = DateTime.UtcNow;
        [DisplayName("Modified on")]
        [JsonIgnore]
        public DateTime? ModifiedOn { get; set; }
        [DisplayName("Deleted on")]
        [JsonIgnore]
        public DateTime? DeletedOn { get; set; }
        [DisplayName("Is Deleted")]
        [JsonIgnore]
        public bool IsDeleted { get; set; }
    }
}
