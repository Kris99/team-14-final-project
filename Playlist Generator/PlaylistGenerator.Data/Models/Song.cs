﻿using PlaylistGenerator.Data.Models.Abstract;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using System.Text.Json.Serialization;

namespace PlaylistGenerator.Data.Models
{
    public class Song : Entity
    {
        [Key]
        public Guid Id { get; set; }

        [Required]
        public string Title { get; set; }

        public Guid ArtistId { get; set; }
        public Artist Artist { get; set; }

        public Guid AlbumId { get; set; }
        public Album Album { get; set; }

        [Required]
        public int Duration { get; set; } 

        [Required]
        public int Rank { get; set; }

        [Required]
        public string URL { get; set; }

        [Required]
        public Genres Genre { get; set; }

        public ICollection<PlaylistItem> Items { get; set; }
    }
}
