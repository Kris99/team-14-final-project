﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace PlaylistGenerator.Data.Models
{
    public class User : IdentityUser<Guid>
    {
        [Required, MinLength(3), MaxLength(50)]
        public string FirstName { get; set; }
        [Required, MinLength(3), MaxLength(50)]
        public string LastName { get; set; }
        [Required]
        public bool IsBanned { get; set; } = false; //I prefer using my own property, because the Identity one just locks out the user and he can't login in the site.
        public DateTime? BanExpirationDate { get; set; }

    }
}
