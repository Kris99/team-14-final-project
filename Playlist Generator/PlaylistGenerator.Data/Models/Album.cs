﻿using PlaylistGenerator.Data.Models.Abstract;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Text.Json.Serialization;

namespace PlaylistGenerator.Data.Models
{
    public class Album : Entity
    {
        [Key]
        public Guid Id { get; set; }
        [Required]
        public string Name { get; set; }
        public string CoverUrl { get; set; }
        public ICollection<Song> Songs { get; set; }

    }
}
