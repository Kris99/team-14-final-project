﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using PlaylistGenerator.Data.Models;
using PlaylistGenerator.Data.Seeding;
using System;

namespace PlaylistGenerator.Data
{
    public class PlaylistGeneratorContext : IdentityDbContext<User, Role, Guid>
    {
        public PlaylistGeneratorContext()
        {

        }
        public PlaylistGeneratorContext(DbContextOptions<PlaylistGeneratorContext> options)
                                                                                    : base(options)
        {

        }


        public DbSet<Artist> Artists { get; set; }
        public DbSet<Album> Albums { get; set; }
        public DbSet<Song> Songs { get; set; }
        public DbSet<Playlist> Playlists { get; set; }

        public int Count()
        {
            throw new NotImplementedException();
        }

        public DbSet<PlaylistItem> PlaylistItem { get; set; }


        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<PlaylistItem>().HasKey(pi => new { pi.SongId, pi.PlaylistId });

            builder.Entity<PlaylistItem>()
                .HasOne<Song>(s => s.Song)
                .WithMany(pl => pl.Items)
                .HasForeignKey(sc => sc.SongId);

            builder.Entity<PlaylistItem>()
                .HasOne<Playlist>(sc => sc.PLaylist)
                .WithMany(s => s.Items)
                .HasForeignKey(sc => sc.PlaylistId);

            var userRole = new Role
            {
                Id = Guid.NewGuid(),
                Name = "User",
                NormalizedName = "USER"
            };
            var adminRole = new Role
            {
                Id = Guid.NewGuid(),
                Name = "Admin",
                NormalizedName = "ADMIN"
            };
            builder.Entity<Role>().HasData(userRole, adminRole);


            var user = new User
            {
                Id = Guid.NewGuid(),
                FirstName = "Kristian",
                LastName = "Dimitrov",
                UserName = "Admin",
                Email = "kris.dimitrov99@gmail.com",
                NormalizedEmail = "KRIS.DIMITROV99@GMAIL.COM",
                SecurityStamp = Guid.NewGuid().ToString(),               
            };
            user.PasswordHash = new PasswordHasher<User>().HashPassword(user, "0809999km");

            builder.Entity<User>().HasData(user);

            builder.Entity<IdentityUserRole<Guid>>().HasData(
                new IdentityUserRole<Guid>
                {
                    RoleId = adminRole.Id,
                    UserId = user.Id
                });

            //uncomment to seed
            //builder.Seed();

            base.OnModelCreating(builder);
        }

    }
}
