﻿using PlaylistGenerator.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace PlaylistGenerator.Data.Seeding.JsonModels
{
    public class SongCollection
    {
        [JsonPropertyName("data")]
        public IEnumerable<JsonSong> Songs { get; set; }

        [JsonPropertyName("total")]
        public int TotalSongs { get;set; }

        [JsonPropertyName("next")]
        public string NextPageUrl { get; set; }


    }
}
