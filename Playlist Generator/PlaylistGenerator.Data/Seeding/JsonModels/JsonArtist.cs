﻿using System.Text.Json.Serialization;

namespace PlaylistGenerator.Data.Seeding.JsonModels
{
    public class JsonArtist
    {
        [JsonPropertyName("id")]
        public long Id { get; set; }

        [JsonPropertyName("name")]
        public string Name { get; set; }

        [JsonPropertyName("link")]
        public string Url { get; set; }

        [JsonPropertyName("picture")]
        public string PictureUrl { get; set; }

        [JsonPropertyName("tracklist")]
        public string SongsUrl { get; set; }
    }
}