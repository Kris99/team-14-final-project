﻿using PlaylistGenerator.Data.Models;
using System;
using System.Text.Json.Serialization;

namespace PlaylistGenerator.Data.Seeding.JsonModels
{
    public class JsonSong
    {
        [JsonPropertyName("id")]
        public long FakeId { get; set; }

        [JsonIgnore]
        public long Id { get; set; }

        [JsonIgnore]
        public Genres Genres { get; set; }

        [JsonPropertyName("title")]
        public string Title { get; set; }

        [JsonPropertyName("link")]
        public string URL { get; set; }

        [JsonPropertyName("duration")]
        public int Duration { get; set; } // in seconds

        [JsonPropertyName("rank")]
        public int Rank { get; set; }

        [JsonPropertyName("preview")]
        public string PreviewUrl { get; set; }

        [JsonPropertyName("artist")]
        public JsonArtist Artist { get; set; }

        [JsonPropertyName("album")]
        public JsonAlbum Album { get; set; }
    }
}