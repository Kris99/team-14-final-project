﻿using System.Text.Json.Serialization;

namespace PlaylistGenerator.Data.Seeding.JsonModels
{
    public class JsonAlbum
    {
        [JsonPropertyName("id")]
        public long Id { get; set; }

        [JsonPropertyName("title")]
        public string Title { get; set; }

        [JsonPropertyName("cover")]
        public string CoverUrl { get; set; }

        [JsonPropertyName("tracklist")]
        public string SongsUrl { get; set; }
    }
}