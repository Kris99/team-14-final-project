﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace PlaylistGenerator.Data.Seeding.JsonModels
{
    public class JsonPlaylist
    {
        [Key]
        [JsonPropertyName("id")]
        public long FakeId { get; set; }

        [JsonPropertyName("title")]
        public string Title { get; set; }

        [JsonPropertyName("tracklist")]
        public string URL { get; set; }
        [JsonPropertyName("user")]
        public JsonUser User { get; set; }
    }
}