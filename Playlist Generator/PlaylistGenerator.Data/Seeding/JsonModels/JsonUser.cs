﻿using System.Text.Json.Serialization;

namespace PlaylistGenerator.Data.Seeding.JsonModels
{
    public class JsonUser
    {
        [JsonPropertyName("id")]
        public long Id { get; set; }
        [JsonPropertyName("name")]
        public string Name { get; set; }
    }
}