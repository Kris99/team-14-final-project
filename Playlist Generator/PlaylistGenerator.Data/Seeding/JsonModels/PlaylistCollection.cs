﻿using PlaylistGenerator.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace PlaylistGenerator.Data.Seeding.JsonModels
{
    public class PlaylistCollection
    {
        [JsonPropertyName("data")]
        public IEnumerable<JsonPlaylist> Playlists { get; set; }

        [JsonPropertyName("total")]
        public int TotalSongs { get; set; }

        [JsonPropertyName("next")]
        public string NextPageUrl { get; set; }
    }
}
