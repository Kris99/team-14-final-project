﻿using Microsoft.EntityFrameworkCore;
using PlaylistGenerator.Data.Models;
using PlaylistGenerator.Data.Seeding.JsonModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;

namespace PlaylistGenerator.Data.Seeding
{

    public static class Seeder
    {
        //private static ICollection<Playlist> playlists = new List<Playlist>();

        public static void Seed(this ModelBuilder builder)
        {
            //pop playlists
            CreatePlaylistsAsync("pop", builder);

            //rock playlists
            CreatePlaylistsAsync("rock", builder);

            //rap playlists
            CreatePlaylistsAsync("rap", builder);

            //folk playlists
            CreatePlaylistsAsync("folk", builder);
        }

        public static void CreatePlaylistsAsync(string genre, ModelBuilder builder)
        {
            var url = $"https://api.deezer.com/search/playlist?q={genre}";
            var client = new HttpClient();

            var response = client.GetAsync(url);
            var collectionJson = response.Result.Content.ReadAsStringAsync();
            var collection = JsonSerializer.Deserialize<PlaylistCollection>(collectionJson.Result);


            List<User> currentUsers = new List<User>();
            List<Playlist> currentPlaylists = new List<Playlist>();
            List<Song> currentSongs = new List<Song>();
            List<Artist> currentArtists = new List<Artist>();
            List<Album> currentAlbums = new List<Album>();
            List<PlaylistItem> currentItems = new List<PlaylistItem>();

            foreach (var jPlaylist in collection.Playlists)
            {
                var user = new User
                {
                    UserName = jPlaylist.User.Name,
                    FirstName = "Seeded",
                    LastName = "User"
                };
            
                Guid userIdForPlaylist;
                var sameUser = currentUsers.FirstOrDefault(u => u.UserName == user.UserName);
            
                if (sameUser == null)
                {
                    user.Id = Guid.NewGuid();
                    currentUsers.Add(user);
                    userIdForPlaylist = user.Id;
                }
                else
                    userIdForPlaylist = sameUser.Id;
            
            
                var playlist = new Playlist
                {
                    Id = Guid.NewGuid(),
                    Title = jPlaylist.Title,
                    URL = jPlaylist.URL,
                    Genres = genre,
                    TotalPlayTime = CalculateTotalTime(jPlaylist.URL),
                    AvrRank = CalculateTotalRank(jPlaylist.URL),
                    UserId = userIdForPlaylist,
                    AddedOn = DateTime.UtcNow,
                    IsDeleted = false
                };

                GetPlaylistsDetails(playlist, currentSongs, currentArtists, currentAlbums, currentItems);

                currentPlaylists.Add(playlist);
                //playlists.Add(playlist);
            }

            builder.Entity<User>().HasData(currentUsers);
            builder.Entity<Playlist>().HasData(currentPlaylists);
            builder.Entity<Song>().HasData(currentSongs);
            builder.Entity<Artist>().HasData(currentArtists);
            builder.Entity<Album>().HasData(currentAlbums);
            builder.Entity<PlaylistItem>().HasData(currentItems);
        }

        private static int CalculateTotalTime(string url)
        {
            var client = new HttpClient();
            var response = client.GetAsync(url);
            var songCollectionJson = response.Result.Content.ReadAsStringAsync();
            var songCollection = JsonSerializer.Deserialize<SongCollection>(songCollectionJson.Result);

            var totalTime = 0;

            foreach (var song in songCollection.Songs)
            {
                totalTime += song.Duration;
            }

            return totalTime;
        }

        private static double CalculateTotalRank(string url)
        {
            var client = new HttpClient();
            var response = client.GetAsync(url);
            var songCollectionJson = response.Result.Content.ReadAsStringAsync();
            var songCollection = JsonSerializer.Deserialize<SongCollection>(songCollectionJson.Result);

            double totalRank = 0;

            foreach (var song in songCollection.Songs)
            {
                totalRank += song.Rank;
            }

            totalRank = Math.Round(totalRank / songCollection.TotalSongs);

            return totalRank;
        }

        private static void GetPlaylistsDetails(Playlist playlist, List<Song> songs, List<Artist> artists,
                                                                List<Album> albums, List<PlaylistItem> items)
        {
            var url = playlist.URL;
            var client = new HttpClient();
        
            var response = client.GetAsync(url);
            var collectionJson = response.Result.Content.ReadAsStringAsync();
            var songCollection = JsonSerializer.Deserialize<SongCollection>(collectionJson.Result);
        
            foreach (var jSong in songCollection.Songs)
            {
                var album = new Album
                {
                    Id = Guid.NewGuid(),
                    Name = jSong.Album.Title,
                    AddedOn = DateTime.UtcNow,
                    CoverUrl = jSong.Album.CoverUrl,
                    IsDeleted = false
                };
                var sameAlbum = albums.FirstOrDefault(a => a.Name == album.Name);

                if (sameAlbum == null)
                    albums.Add(album);

                var artist = new Artist
                {
                    Id = Guid.NewGuid(),
                    Name = jSong.Artist.Name,
                    PictureUrl = jSong.Artist.PictureUrl,
                    AddedOn = DateTime.UtcNow,
                    IsDeleted = false
                };
                var sameArtist = artists.FirstOrDefault(a => a.Name == artist.Name);

                if (sameArtist == null)
                    artists.Add(artist);

                var song = new Song
                {
                    Id = Guid.NewGuid(),
                    AddedOn = DateTime.UtcNow,
                    Title = jSong.Title,
                    AlbumId = album.Id,
                    ArtistId = artist.Id,
                    Duration = jSong.Duration,
                    Rank = jSong.Rank,
                    URL = jSong.PreviewUrl,
                    IsDeleted = false,
                    Genre = (Genres)Enum.Parse(typeof(Genres), playlist.Genres, true)
                };
                var sameSong = songs.FirstOrDefault(s => s.Title == song.Title);

                if (sameSong == null)
                {
                    songs.Add(song);

                    var item = new PlaylistItem
                    {
                        SongId = song.Id,
                        PlaylistId = playlist.Id
                    };
                    items.Add(item);
                }
            }
        }
    }
}
