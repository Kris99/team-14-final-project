# Team 14 Final Project

Trello board: https://trello.com/b/SqieNDTM/team-14-final-project

Hosted on Azure: https://playlistgenerator20201209140021.azurewebsites.net/


# RidePal
Enables our users to be able to generate playlists for specific travel duration
periods based on their preferred genres.

# Technologies used
- ASP .NET Core
- Entity Framework Core
- ASP .NET Identity
- MS SQL Server
- Bing Maps API
- Deezer API
- Bootstrap
- AutoMapper
- jQuery
- Swagger
- CSS


# Views from the web application

## Homepage
![Text that reveals a missing image](Playlist Generator/ReadMeImages/HomePage.png)


## Login 
![Text that reveals a missing image](Playlist Generator/ReadMeImages/LoginPage.png)


## Register
![Text that reveals a missing image](Playlist Generator/ReadMeImages/Register.png)


## Generate Playlist
![Text that reveals a missing image](Playlist Generator/ReadMeImages/GeneratePlaylist.png)


## All Playlists
![Text that reveals a missing image](Playlist Generator/ReadMeImages/AllPlaylists.png)


## Song List
![Text that reveals a missing image](Playlist Generator/ReadMeImages/SongsInPlaylist.png)


## Song Details
![Text that reveals a missing image](Playlist Generator/ReadMeImages/SongDetails.png)

## Users
![Text that reveals a missing image](Playlist Generator/ReadMeImages/UserPage.png)

## Datatable 
![Text that reveals a missing image](Playlist Generator/ReadMeImages/Datatable.png)